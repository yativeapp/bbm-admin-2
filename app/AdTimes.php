<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdTimes extends Model
{
    
    protected $fillable = ['broadcast_at'];

}
