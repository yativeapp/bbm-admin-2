<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{


    protected $fillable = ['addressable_id', 'building_no', 'district1', 'district2',  'street', 'city', 'gov'];
}
