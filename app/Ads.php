<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    

    protected $fillable = ['cover' , 'cover_type' ,'car_types' , 'content' ,'title', 'crons'];

    protected $casts = ['crons' => 'json' , 'car_types' => 'json'];

}
