<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name' , 'icon'
    ];

    public function scopeFilter($query, $filters)
    {

        return $filters->apply($query);
    }


    public function getIconAttribute($val)
    {
         
        if(is_null($val) || $val == ""){
            return 'https://ui-avatars.com/api/?background=CE00DB&color=fff&rounded=true&name=' . $this->name;
        }

        return $val;
    }
}
