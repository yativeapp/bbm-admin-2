<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Card extends Model
{
     use SoftDeletes;
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'card_type_id', 'prev_package_id', 'pos_agent', 'user_id', 'issued_by', 'package_id',  'valid_thru', 'number', 'verified', 'created_at'
     ];

     protected $appends = ['valid_for'];

     protected $dates = ['valid_thru'];



     /**
      * The "booted" method of the model.
      *
      * @return void
      */
     protected static function boot()
     {
          parent::boot();

          static::deleting(function ($card) {


               $pointsUsed = $card->user->sales->whereBetween('created_at', [now(), $card->created_at])->count();

               $pointsToDeduct = ($card->package->points - $pointsUsed) < 0 ? 0 : $card->package->points - $pointsUsed;

               $points = ($card->user->points - $pointsToDeduct) < 0 ? 0 : $pointsToDeduct;

               $card->user->decrement('points', $points);
          });
     }


     public function getValidForAttribute()
     {

          return $this->created_at->addDays(3);
     }

     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }


     public function scopePos($query)
     {
          return $query->where('pos_agent', '!=', null);
     }

     public function user()
     {

          return $this->belongsTo(User::class);
     }

     public function posAgent()
     {

          return $this->belongsTo(User::class, 'pos_agent');
     }
     public function cardType()
     {

          return $this->belongsTo(CardType::class);
     }
     public function package()
     {

          return $this->belongsTo(CardtypePackage::class);
     }
}
