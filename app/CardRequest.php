<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CardRequest extends Model
{

    use SoftDeletes;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'card_type_id','package_id', 'valid_thru', 'car_type', 'payment_method', 'address' , 'receive_type' , 'moved_to_pos_at'
    ];

    protected $with = ['cardType' , 'package'];

    protected $dates = ['moved_to_pos_at' , 'valid_thru'];

    public function scopeFilter($query, $filters)
    {

        return $filters->apply($query);
    }

    public function user()
    {

        return $this->belongsTo(User::class);
    }
    public function cardType()
    {

        return $this->belongsTo(CardType::class);
    }
    public function package()
    {

        return $this->belongsTo(CardtypePackage::class);
    }


    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }
}
