<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CardType extends Model
{

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'name', 'type', 'description', 'cover', 'for' , 'bgColor' , 'logo'
     ];



     public function cards()
     {

          return $this->hasMany(Card::class);
     }
     public function packages()
     {

          return $this->hasMany(CardtypePackage::class);
     }

     public function offers()
     {

          return $this->belongsToMany(Offer::class);
     }

     public function allOffers()
     {
          return $this->belongsToMany(Offer::class)->withoutGlobalScope('range');
     }


     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }

     public function merchants()
     {
          return $this->belongsToMany(User::class);
     }

     public function categories()
     {

          return $this->belongsToMany(Category::class)->distinct();
     }
}
