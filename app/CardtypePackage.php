<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardtypePackage extends Model
{

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'card_type_id', 'points', 'name', 'price' , 'in_stock'
     ];

     protected $casts = ['in_stock' => 'boolean'];

     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }

     public function cardType()
     {

          return $this->belongsTo(CardType::class);
     }
}
