<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'name', 'icon', 'icon_type'
     ];


     public function merchants()
     {

          return $this->belongsToMany(User::class);
     }
     public function cardTypes()
     {

          return $this->belongsToMany(CardType::class);
     }

     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }
}
