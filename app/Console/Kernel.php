<?php

namespace App\Console;

use App\Ads;
use App\Extras\AdCron;
use App\Extras\Bookings;
use App\Extras\CardExpireCron;
use App\Extras\FCMProvider;
use App\Extras\Offers;
use App\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Str;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


        $schedule->call(function () {

            (new AdCron)->execute();
            (new Bookings)->execute();
        })->everyMinute();
        $schedule->call(function () {

            (new CardExpireCron)->execute(true);
        })->dailyAt('12:00');

        $schedule->call(function () {

            (new CardExpireCron)->execute(false);
        })->dailyAt('00:30');

        $schedule->call(function () {

            (new Offers)->execute(0);
        })
            ->timezone('Africa/Cairo')
            ->dailyAt('09:00');
        $schedule->call(function () {

            (new Offers)->execute(1);
        })
            ->timezone('Africa/Cairo')
            ->dailyAt('11:00');
        $schedule->call(function () {

            (new Offers)->execute(2);
        })
            ->timezone('Africa/Cairo')
            ->dailyAt('13:00');
        $schedule->call(function () {

            (new Offers)->execute(3);
        })
            ->timezone('Africa/Cairo')
            ->dailyAt('16:00');
        $schedule->call(function () {

            (new Offers)->execute(4);
        })
            ->timezone('Africa/Cairo')
            ->dailyAt('20:00');

        // $schedule->exec('sudo mysql -u root -p "" bbm > /var/www/html/bbm/backups/bbm.sql')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
