<?php

namespace App\Exports;

use App\CardtypePackage;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class MerchantsExport implements FromCollection, WithHeadings, WithMapping
{

    public $packages;


    public function __construct()
    {
        $this->packages = CardtypePackage::where('card_type_id' , request('card_type'))->oldest()->limit(7)->get();

    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $relations = [
            'offers.categories' => function ($b) {
                return $b->whereHas('cardTypes', function ($query) {

                    return $query->where('card_types.id', request('card_type'));
                })->latest()->limit(10);
            } , 
            'company',
            'offers' => function ($b) {
                return $b->with(['discounts' => function($q){
                    $q->where('offer_discounts.card_type_id' , request('card_type'));
                }]);
           
            },
            'addresses' => function ($b) {
                return $b->oldest()->limit(1);
            },
            'branches' => function($b){ return $b->oldest()->limit(1);}
        ];

        return User::whereHas('cardTypes', function ($query) {

            return $query->where('card_types.id', request('card_type'));
        })->merchants()->with($relations)->get();
    }


    public function map($merchant): array
    {


        $rowNum = substr($merchant->serial,11,4);

   
        $date = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($merchant->created_at);


        $address = [];

        if($merchant->branches->count()){

            $branch = $merchant->branches->first();

            $address = explode(" " , $branch->address_title);

        }

        $hasAddress = $merchant->addresses->count();

        $address = $merchant->addresses->first();

        $data = [
            $rowNum,
            $merchant->fullname,
            "{$merchant->serial}",
            $date,
            $merchant->company ? $merchant->company->representee : "",
            $merchant->company ? $merchant->company->type : "",
            $merchant->acting ? $merchant->company->acting : "",
            isset($address[0]) ? $address[0] : "",
            isset($address[1]) ? $address[1] : "",
            isset($address[2]) ? $address[2] : "",
            isset($address[3]) ? $address[3] : "",
            isset($address[4]) ? $address[4] : "",
            $merchant->company ? $merchant->company->landline : "",
            $merchant->company ? $merchant->company->mobile : "",
            $merchant->company ? $merchant->company->mobile2 : "",
            $merchant->owner_name,
            $merchant->job,
            $hasAddress ? $address->building_no : "",
            $hasAddress ? $address->street : "",
            $hasAddress ? $address->district1 : "",
            $hasAddress ? $address->city : "",
            $hasAddress ? $address->gov : "",
            $merchant->id_no,
            $merchant->landline,
            $merchant->mobile,
            $merchant->mobile2,
        ];
    
    

        $merchant->offers->each(function($offer) use (&$data){

            $data[] = $offer->categories->count() ? $offer->categories->first()->name : "";

            $data[] = $offer->discount;
            $data[] = $offer->title;
            $data[] = $offer->description;

            //address
            $data[] = "";


            $this->packages->pad(12, "")->each(function($p) use ($offer,&$data){

                if($p == ""){
                    $data[] = "FALSE";
                }else{

                    if($offer->discounts->pluck('package_id')->contains($p->id)){
                        $data[] = "TRUE";
                    }else{
                        $data[] = "FALSE";
                    }
                }

            });

        });

        return $data;
    }

    public function headings(): array
    {


        $columns =  Collection::times(10, function () {
            return collect([
                'Categories',
                'Discount',
                'Short Description',
                'Full Description',
                'العنوان'
            ])
            ->merge($this->packages->pluck('name'))
            ->pad(12, "")->toArray();
        });
  
        
        return array_merge([
            '',
            'شركة',
            'Service Provider Serial',
            'تاريخ',
            'ممثل الشركة',
            'صفتها',
            'نشاطها',
            'عقار رقم',
            'شارع',
            'منطقة 1',
            'منطقة 2',
            'محافظة',
            'تليفون أرضي',
            'موبايل 1',
            'موبايل 2',
            // merchant address
            'عقار رقم',
            'شارع',
            'منطقة',
            'مدينة',
            'محافظة',
            //
            'الرقم القومي',
            'تليفون أرضي',
            'موبايل 1',
            'موبايل 2'
        ] , $columns->flatten()->all());
    }
}
