<?php

namespace App\Exports;

use App\Card;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $relations = ['cards.package' => function ($q) {
            return $q->limit(1)->oldest();
        }];


        return User::customers()->with($relations)->get();
    }



    public function map($user): array
    {

        $card = $user->cards->first();

        $sn = "";

        $package = "";

        $date = "";

        $issuedBy = "";

        if($card){
            $sn = $card->number;

            $sn = substr($sn, 0, 4) . " " . substr($sn, 4, 4) . " " . substr($sn, 8, 4) . " " . substr($sn, 12, 4);
            
            $package = $card->package ? $card->package->name :  "";


            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($card->created_at);

            $issuedBy = $card->issued_by;
        }



        return [
            $user->fullname,
            $sn,
            $date, // issued on 
            $package,
            $issuedBy, //issued by
            $user->fullname,
            $user->id_no, // ID
            $user->address,
            "", // district,
            $user->job,
            $user->telephone,
            $user->mobile,
            $user->mobile2,
            $user->email,
            $user->car_no,
            $user->car_type,
            "", //printing,
            "", // estaml 3la,
            "", //Delivery Through,
            "", //تاريخ تسلي
        ];
    }

    public function headings(): array
    {
        return [
            'Card Holder Name',
            'SN',
            'Issued ON',
            'Member',
            'Issued By',
            'Full Name',
            'ID',
            'Address',
            'منطقة',
            'job',
            'land_line',
            'mobile_1',
            'mobile_2',
            'email',
            'Car No.',
            'Brand',
            'Printing',
            'إستلام علا',
            'Delivery Through',
            'تاريخ تسليم',
            'Status',
            'Delivered on',
            'Activation',
            'Activation Date',
            'Notes'
        ];
    }
}
