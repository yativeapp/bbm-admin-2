<?php

namespace App\Extras;



class Accept
{


     private $token = "";

     private $merchant_id = "";

     private $order_id = "";

     private $payKey = "";



     public function getAuthToken()
     {
          $client = new \GuzzleHttp\Client();

          $endpoint = 'https://accept.paymobsolutions.com/api/auth/tokens';

          $key = "ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SndjbTltYVd4bFgzQnJJam8wTWpFMExDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MElpd2libUZ0WlNJNkltbHVhWFJwWVd3aWZRLlRKOWttUzZJbnlYTTFtb3VkZzhIYVQ3eEIyZk5LZFVFcDE4dG1EVk05VDlFM2UyZU8tSEQwRGtHRkIzeFh1RUtmYUpiSWEzcWVCcjhabktiT282N2pR";




          $response = $client->post(
               $endpoint,
               [
                    'json' => ["api_key" => $key],
                    'headers' => [
                         'Content-Type'     => 'application/json',

                    ]
               ]
          );

          $data = json_decode($response->getBody());

          $this->token =  $data->token;

          $this->merchant_id = $data->profile->id;
     }


     public function order($amount)
     {
          $client = new \GuzzleHttp\Client();

          $endpoint = 'https://accept.paymobsolutions.com/api/ecommerce/orders';



          $response = $client->post(
               $endpoint,
               [
                    'json' => [
                         "auth_token" => $this->token,
                         "delivery_needed" => "false",
                         "merchant_id" => $this->merchant_id,
                         "amount_cents" => $amount,
                         "currency" => "EGP",
                         "items" => [],

                    ],
                    'headers' => [
                         'Content-Type'     => 'application/json',

                    ]
               ]
          );

          $data = json_decode($response->getBody());

          $this->order_id = $data->id;
     }

     public function payKey($amount)
     {

          $client = new \GuzzleHttp\Client();

          $endpoint = 'https://accept.paymobsolutions.com/api/acceptance/payment_keys';



          $response = $client->post(
               $endpoint,
               [
                    'json' => [

                         "auth_token" => $this->token,
                         "amount_cents" => $amount,
                         "expiration" => $this->order_id,
                         "billing_data" => [
                              "apartment" => "803",
                              "building" => "8028",
                              "country" => "EG",
                              "street" => "cairo",
                              "floor" => "42",
                              "city" => "cairo",

                              "email" => auth()->user()->email,
                              "first_name" => auth()->user()->fullname,
                              "last_name" => auth()->user()->fullname,
                              "phone_number" => auth()->user()->mobile,
                         ],
                         "currency" => "EGP",
                         "integration_id" => 6741,
                         "lock_order_when_paid" => "true"

                    ],
                    'headers' => [
                         'Content-Type'     => 'application/json',

                    ]
               ]
          );

          $data = json_decode($response->getBody());

          $this->payKey = $data->token;
     }


     public function charge($amount)
     {

          $this->getAuthToken();

          $this->order($amount);

          $this->payKey($amount);
     }

     public function getIFrame()
     {

          return "https://accept.paymobsolutions.com/api/acceptance/iframes/10785?payment_token=" . $this->payKey;
     }
}
