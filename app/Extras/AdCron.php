<?php

namespace App\Extras;

use App\Notifications\Ad;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;
use Log;

class AdCron
{


     private $users = null;

     public function execute()
     {
          return DB::table('ads')
               ->chunkById(30, function ($ads) {

                    collect($ads)
                         ->filter(function ($ad) {

                              return collect(json_decode($ad->crons))->filter(function ($cron) {

                                   $now = now()->setTimezone("Africa/Cairo");


                                   list($date, $time) = explode(' ', $cron);
                                   list($day, $month, $year) = explode('-', $date);
                                   list($hour, $minute) = explode(':', $time);


                                   $nowHour = str_pad($now->hour, 2, "0", STR_PAD_LEFT);
                                   $nowMin = str_pad($now->minute, 2, "0", STR_PAD_LEFT);


                                   return   $now->year == $year &&
                                        strtolower($now->dayName) == strtolower($day) &&
                                        strtolower($now->monthName) == strtolower($month) &&
                                        $nowHour == $hour && $nowMin == $minute;
                              })->count();
                         })->each(function ($ad) {

                              $this->send($ad);
                         });
               });
     }


     public function send($ad)
     {



          $users = $this->getCachedUsers();

          if ($ad->car_types) {

               $types = json_decode($ad->car_types);

               if (sizeof($types) > 0) {

                    $users = $users->filter(function ($user) use ($types) {
                         return $user->car_types->whereIn('name', $types)->count();
                    })->get();
               }
          }



          $users->each(function ($user) use ($ad) {
               $user->notify(new Ad($ad));
          });


          $fcm = new FCMProvider;



          $fcm->title($ad->title && !empty($ad->title)? $ad->title : 'BBM')
               ->body(Str::limit($ad->content, 15))
               ->data(['id' => $ad->id,'title' =>$ad->title ,  'content' => Str::limit($ad->content, 15)], 'ad')
               ->send($users->pluck('token')->toArray());
     }


     public function getCachedUsers()
     {
          if ($this->users) {
               return $this->users;
          }

          $this->users = User::with('carTypes')->select(['id', 'token'])->where('token', '!=', null)->get();

          return $this->users;
     }
}
