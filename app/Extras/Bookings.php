<?php

namespace App\Extras;

use App\OfferBooking;

class Bookings
{


     public function execute()
     {

          $now = now()->tz("Africa/Cairo");
          OfferBooking::with(['offer', 'user'])->whereDate(now())->get()->each(function ($booking) use ($now) {



               if ($now->isAfter($booking->booking_at) &&   $now->diffInMinutes($booking->booking_at) == 30) {

                    $fcm = new FCMProvider;



                    $fcm->title($booking->offer->merchant->fullname . " Booking")
                         ->body("Your Booking is in 30 mins , be ready")
                         ->body([], 'bookings')
                         ->send([$booking->user->token]);
               }
          });
     }
}
