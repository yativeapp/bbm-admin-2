<?php

namespace App\Extras;

use App\Card;

class CardExpireCron
{


     public function execute($notifyOnly)
     {



          Card::with(['user', 'package'])->chunkById(100, function ($cards) use ($notifyOnly) {


               foreach ($cards as $card) {

                    $fcm = new FCMProvider;

                    $days = $card->valid_thru->diffInDays(now());



                    if ($days <= 7 && $notifyOnly) {
                         $in = "In " . $days . " Days";

                         $fcm->title('BBM')
                              ->body('Your Card with this Number ' . $card->number . ' is about to expire ' . $in)
                              ->data([], 'expired-card')
                              ->send($card->user->token);
                    }

                    if ($card->valid_thru->isPast() && !$notifyOnly) {

                         $card->delete();
                    }
               }
          });
     }
}
