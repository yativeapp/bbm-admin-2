<?php

namespace App\Extras;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

use FCM;

class FCMProvider
{

     private $dataBuilder;

     private $notificationBuilder;

     private $type;

     private $title;

     private $body;


     private $silent = false;


     public function title($title = '')
     {
          $this->title = $title;

          return $this;
     }

     public function body($body = '')
     {
          $this->body = $body;

          return $this;
     }
     public function silent($bool)
     {
          $this->silent = $bool;

          return $this;
     }
     public function data($data = [], $type = '')
     {

          $data = ['type' => $type, 'body' => $this->body, 'title' => $this->title, 'data' => $data];


          $this->dataBuilder = new PayloadDataBuilder();
          $this->dataBuilder->addData($data);

          return $this;
     }



     public function send($token)
     {



          if (is_string($token) || is_null($token)) {

               $token = [$token];
          }

          $token = array_filter($token);



          if (count($token) == 0) return;

          $optionBuilder = new OptionsBuilder();
          $optionBuilder->setTimeToLive(60 * 20);

          $option = $optionBuilder->build();


          $data = $this->dataBuilder->build();


          $notification = null;

          if(!$this->silent){
               $notificationBuilder = new PayloadNotificationBuilder($this->title);
               $notificationBuilder->setBody($this->body)->setClickAction('FCM_PLUGIN_ACTIVITY');
               $notification = $notificationBuilder->build();
          }

       

     

          FCM::sendTo($token, $option, $notification , $data);
     }
}
