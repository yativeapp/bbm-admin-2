<?php

namespace App\Extras;

use App\Offer;
use App\User;
use App\Notifications\Offer as NotificationsOffer;


class Offers
{


     public function execute($limit = 0)
     {
          $users = User::customers()->select(['id', 'token'])->get();

          Offer::with(['branches', 'merchant'])
               ->whereDay('created_at', '=', date('d'))
               ->latest()
               ->offset($limit * 2)
               ->limit(2)
               ->get()->filter(function ($offer) {
                    return !$offer->expired;
               })->each(function ($offer) use ($users) {
                    $this->send($offer, $users);
               });
     }


     public function send($offer, $users)
     {


          $fcm = new FCMProvider;

          $branch = $offer->branches->first() ? $offer->branches->first()->name : "";

          $title = $offer->merchant->fullname . " -  " . $branch;

          $fcm->title($title)
               ->body($offer->description)
               ->data(['id' => $offer->id], 'offer')
               ->send($users->pluck('token')->toArray());


          $users->each(function ($user) use ($offer, $title) {
               $user->notify(new NotificationsOffer($offer, $title));
          });
     }
}
