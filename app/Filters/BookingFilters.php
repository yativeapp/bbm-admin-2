<?php

namespace App\Filters;



class BookingFilters extends Filters
{

     protected $filters = ['with', 'offer_id', 'filterByCategory', 'merchant_id', 'date', 'rangeDate'];



     public function date($val)
     {
          $this->builder->whereDate('booking_at', \Carbon\Carbon::parse($val));
     }
     public function rangeDate($val)
     {
          $date = \Carbon\Carbon::parse($val);

          $this->builder->whereYear('booking_at', $date->year)->whereMonth('booking_at', $date->month);
     }


     public function merchant_id($val)
     {


          return $this->builder->whereHas('offer', function ($query) use ($val) {
               return $query->where('offers.merchant_id', $val);
          });
     }
     public function offer_id($val)
     {


          return $this->builder->whereHas('offer', function ($query) use ($val) {
               return $query->where('offers.id', $val);
          });
     }

     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }
}
