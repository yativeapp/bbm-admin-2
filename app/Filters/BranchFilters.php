<?php

namespace App\Filters;



class BranchFilters extends Filters
{

     protected $filters = ['name', 'limit', 'email', 'orderBy', 'count', 'with', 'offer_id', 'filterByCategory', 'merchant_ids'];


     public function name($val)
     {

          return $this->builder->where('fullname', 'like', "%{$val}%");
     }
     public function email($val)
     {

          return $this->builder->where('email', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function filterByCategory($val)
     {


          return $this->builder->whereHas('categories', function ($query) use ($val) {
               return $query->whereIn('categories.id', explode(',', $val));
          });
     }
     public function merchant_ids($val)
     {


          return $this->builder->whereHas('merchant', function ($query) use ($val) {
               return $query->whereIn('users.id', explode(',', $val));
          });
     }
     public function offer_id($val)
     {


          return $this->builder->whereHas('offers', function ($query) use ($val) {
               return $query->where('offers.id', $val);
          });
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }
     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }
}
