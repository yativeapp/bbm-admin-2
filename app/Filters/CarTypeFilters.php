<?php

namespace App\Filters;

use DB;

class CarTypeFilters extends Filters
{

     protected $filters = ['name', 'limit', 'orderBy', 'count', 'with'];






     public function name($val)
     {

          return $this->builder->where('name', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }


     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }

     public function has($val)
     {


          return $this->builder->has($val);
     }
}
