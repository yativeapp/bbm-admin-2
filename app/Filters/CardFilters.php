<?php

namespace App\Filters;

use DB;

class CardFilters extends Filters
{

     protected $filters = ['name', 'mobile', 'card_number', 'limit', 'orderBy', 'count', 'with', 'latest', 'withExpired'];


     public function name($val)
     {

          return $this->builder->whereHas('user', function ($q) use ($val) {
               return $q->where('users.fullname', 'like', "%{$val}%");
          });
     }
     public function mobile($val)
     {

          return $this->builder->whereHas('user', function ($q) use ($val) {
               return $q->where('users.mobile',  $val)->orWhere('users.mobile2', $val);
          });
     }
     public function card_number($val)
     {

          return $this->builder->where('number', $val);
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }

     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }
     public function withExpired($val)
     {


          return $this->builder->withTrashed();
     }
     public function latest($val)
     {


          return $this->builder->latest();
     }
}
