<?php

namespace App\Filters;

use DB;

class CardTypeFilters extends Filters
{

     protected $filters = ['name', 'targeting', 'limit', 'orderBy', 'count', 'with', 'has', 'notGift', 'withOfferCover'];




     public function targeting()
     {

          $paid_user = auth()->user()->cards()->whereHas('cardType', function ($query) {
               return $query->where('card_types.type', 'normal');
          })->count();


          $param = $paid_user ? 'paid_users' : 'new_users';

          return $this->builder->where('for', $param);
     }

     public function name($val)
     {

          return $this->builder->where('name', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }


     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }

     public function has($val)
     {


          return $this->builder->has($val);
     }

     public function notGift()
     {
          $this->builder->where('type', '!=', 'gift');
     }
     public function withOfferCover()
     {
          $this->builder->with(['merchants.offers' => function ($q) {
               return $q->latest()->limit(1);
          }]);
     }
}
