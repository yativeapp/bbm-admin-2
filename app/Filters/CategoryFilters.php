<?php

namespace App\Filters;

use DB;

class CategoryFilters extends Filters
{

     protected $filters = ['name', 'limit', 'orderBy', 'count' ,'with', 'cardType'];


     public function name($val)
     {

          return $this->builder->where('name', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }
     public function cardType($val)
     {

          if($val === 'all') return $this->builder;
          

          return $this->builder->whereHas('cardTypes' , function($q) use ($val){
              return $q->where('card_types.id' , $val);
          });
     }
}
