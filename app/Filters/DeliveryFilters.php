<?php

namespace App\Filters;

use DB;

class DeliveryFilters extends Filters
{

     protected $filters = ['authed' , 'merchant' , 'with' , 'orderBy'];


     public function authed()
     {
          if(!auth()->check()) return $this->builder;

        return $this->builder->where('user_id' , auth()->id());   
     }
     public function merchant()
     {
          if(!auth()->check()) return $this->builder;

        return $this->builder->where('merchant_id' , auth()->id());   
     }

     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }

     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);

          return $this->builder->orderBy($col, $order);
     }

}