<?php

namespace App\filters;

use Illuminate\Http\Request;

class Filters
{

     protected $request;

     protected $builder;

     protected $model;


     public function __construct(Request $request)
     {

          $this->request = $request;
     }


     public function apply($builder)
     {
          $this->builder = $builder;



          foreach ($this->filters as $filter) {

               $this->callFilter($filter);
          }
     }

     protected function callFilter($filter)
     {

          if ($this->hasFilter($filter) && $parameter = $this->request->get($filter)) {


               if (!is_null($parameter) && $parameter !== 'null' && !empty($parameter)) {

                    $this->$filter($parameter);
               }
          }
     }

     protected function hasFilter($filter)
     {
          return method_exists($this, $filter);
     }
}
