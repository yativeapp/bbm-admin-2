<?php

namespace App\Filters;



class MerchantFilters extends Filters
{




     protected $filters = ['name', 'random', 'hasBookings', 'latest', 'brand', 'filterByBranchLocation2', 'filterByBranchLocation', 'serial', 'mobile', 'limit', 'email', 'orderBy', 'count', 'with', 'filterByCategory', 'filterByCardType', 'filterByCarType', 'trending', 'withLastOffer'];


     public function name($val)
     {

          return $this->builder->where('fullname', 'like', "%{$val}%");
     }
     public function email($val)
     {

          return $this->builder->where('email', 'like', "%{$val}%");
     }
     public function mobile($val)
     {

          return $this->builder->where('mobile', 'like', "%{$val}%");
     }
     public function serial($val)
     {

          return $this->builder->where('serial', $val);
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function latest($val)
     {

          return $this->builder->latest();
     }
     public function filterByCategory($val)
     {

          if ($val === 'all') return $this->builder;

          return $this->builder->whereHas('categories', function ($query) use ($val) {
               return $query->whereIn('categories.id', explode(',', $val));
          });
     }
     public function filterByCardType($val)
     {

          if ($val === 'all') return $this->builder;

          return $this->builder->whereHas('cardTypes', function ($query) use ($val) {

               return $query->whereIn('card_types.id', explode(',', $val));
          });
     }
     public function filterByCarType($val)
     {

          if ($val === 'all') return $this->builder;

          return $this->builder->whereHas('carTypes', function ($query) use ($val) {

               return $query->where('car_types.id', $val);
          });
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }
     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }

     public function trending()
     {

          return  $this->builder->withCount('sales')->orderBy('sales_count', 'desc')->orderBy('top', 'desc');
     }
     public function brand()
     {

          return  $this->builder->where('brand', true);
     }

     public function filterByBranchLocation($val)
     {

          if (empty($val) || is_null($val)) return $this->builder;

          list($lat, $lng) = explode(',', $val);

          return $this->builder->whereHas('branches', function ($query) use ($lat, $lng) {
               return $query->distance($lat, $lng)->having('distance', '<=', 5);
          });
     }

     public function filterByBranchLocation2($val)
     {

          if (empty($val) || is_null($val)) return $this->builder;


          return $this->builder->whereHas('branches', function ($query) use ($val) {
               return $query->where('merchant_branches.id',  $val);
          });
     }

     public function random()
     {
          return $this->builder->distinct()->inRandomOrder();
     }

     public function hasBookings()
     {
          return $this->builder->whereHas('offers', function ($query) {
               $query->withoutGlobalScopes()->has('bookings');
          });
     }

     public function withLastOffer()
     {

          return $this->builder->with(['offers' => function ($q) {
               return $q->latest()->limit(1);
          }]);
     }
}
