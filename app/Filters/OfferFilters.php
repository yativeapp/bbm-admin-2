<?php

namespace App\Filters;



class OfferFilters extends Filters
{

     protected $filters = ['name', 'limit', 'branch_id', 'random', 'filterByBranchLocation2', 'filterByBranchLocation', 'sos', 'latest', 'email', 'withSos', 'orderBy', 'count', 'with', 'filterByCardType', 'filterByCategory', 'merchant_ids', 'trending', 'merchant'];


     public function name($val)
     {

          return $this->builder->orWhere('title', 'like', "%{$val}%")
               ->orWhere('description', 'like', "%{$val}%")
               ->orWhereHas('branches', function ($query) use ($val) {
                    return $query->where('merchant_branches.name', 'like',  "%{$val}%");
               })->with(['branches' => function ($query) use ($val) {
                    return $query->where('name', 'like',  "%{$val}%");
               }]);
     }
     public function email($val)
     {

          return $this->builder->where('email', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function latest($val)
     {

          return $this->builder->latest();
     }

     public function random()
     {
          return $this->builder->distinct()->inRandomOrder();
     }
     public function sos($val)
     {

          return $this->builder->where('sos', true);
     }
     public function withSos($val)
     {

          return $this->builder->withoutGlobalScope('withOutSos');
     }
     public function filterByCategory($val)
     {

          if ($val === 'all') return $this->builder;


          return $this->builder->whereHas('categories', function ($query) use ($val) {
               return $query->whereIn('categories.id', explode(',', $val));
          });
     }
     public function filterByCardType($val)
     {


          if ($val === 'all') return $this->builder;


          return $this->builder->whereHas('cardTypes', function ($query) use ($val) {
               return $query->whereIn('card_types.id', explode(',', $val));
          });
     }
     public function merchant_ids($val)
     {


          return $this->builder->whereHas('merchant', function ($query) use ($val) {
               return $query->whereIn('users.id', explode(',', $val));
          });
     }
     public function merchant($val)
     {
          return $this->builder->whereHas('merchant', function ($query) use ($val) {
               return $query->where('users.fullname', 'like', "%{$val}%");
          });
     }
     public function branch_id($val)
     {


          if (is_null($val)) return;

          return $this->builder->whereHas('branches', function ($query) use ($val) {
               return $query->where('merchant_branch_offer.merchant_branch_id',  $val);
          });
     }

     public function filterByBranchLocation($val)
     {

          if (empty($val) || is_null($val)) return $this->builder;

          list($lat, $lng) = explode(',', $val);

          return $this->builder->whereHas('branches', function ($query) use ($lat, $lng) {
               return $query->distance($lat, $lng)->having('distance', '<=', 5);
          });
     }
     public function filterByBranchLocation2($val)
     {

          if (empty($val) || is_null($val)) return $this->builder;


          return $this->builder->whereHas('branches', function ($query) use ($val) {
               return $query->where('merchant_branch_offer.merchant_branch_id',  $val);
          });
     }

     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);

          if ($col == 'merchant') {
               return  $this->builder->with(['merchant' => function ($q) use ($order) {
                    $q->orderBy('fullname', $order);
               }]);
          } else {
               return $this->builder->orderBy($col, $order);
          }
     }
     public function count($val)
     {
          if ($val == "offers,branches") return $this->builder;

          return $this->builder->withCount(explode(',', $val));
     }
     public function with($val)
     {

          if ($val == "offers,branches") return $this->builder;


          return $this->builder->with(explode(',', $val));
     }

     public function trending()
     {

          return $this->builder->withCount('sales')->orderBy('sales_count', 'desc');
     }
}
