<?php

namespace App\Filters;

use DB;

class PackageFilters extends Filters
{

     protected $filters = ['name', 'limit', 'orderBy', 'count', 'with', 'cardTypeId'];


     public function name($val)
     {

          return $this->builder->where('name', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }

     public function cardTypeId($val)
     {

          return $this->builder->where('card_type_id', explode(',', $val));
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }

     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }
}
