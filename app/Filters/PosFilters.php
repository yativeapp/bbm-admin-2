<?php

namespace App\Filters;

use DB;

class PosFilters extends Filters
{

     protected $filters = ['with' , 'pos_agent' , 'user'];


     public function name($val)
     {

          return $this->builder->whereHas('user', function ($q) use ($val) {
               return $q->where('users.fullname', 'like', "%{$val}%");
          });
     }



     public function with($val)
     {


          return $this->builder->with(explode(',', $val));
     }


     public function pos_agent($val)
     {
          return $this->builder->whereHas('posAgent', function ($query) use ($val) {
               return $query->where('fullname', 'like', "%{$val}%");
          });
     }
     public function user($val)
     {
          return $this->builder->whereHas('user', function ($query) use ($val) {
               return $query->where('fullname', 'like', "%{$val}%");
          });
     }

}