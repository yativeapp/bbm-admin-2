<?php

namespace App\Filters;



class SaleFilters extends Filters
{

     protected $filters = ['user', 'merchant', 'authed', 'bookings'];


     public function authed($val)
     {
          return auth()->check() ? $this->builder->where('merchant_id', auth()->id()) : $this->builder;
     }
     public function user($val)
     {
          return $this->builder->whereHas('user', function ($q) use ($val) {
               return $q->where('type', 'user')->where('fullname', 'like', "%{$val}%");
          });
     }
     public function merchant($val)
     {
          return $this->builder->whereHas('merchant', function ($q) use ($val) {
               return $q->where('type', 'merchant')->where('fullname', 'like', "%{$val}%");
          });
     }

     public function bookings($val)
     {

          return $this->builder->whereHas('offer', function ($query) use ($val) {
               $query->withoutGlobalScopes()->where('bookable', $val == "true");
          });
     }
}
