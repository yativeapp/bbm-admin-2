<?php

namespace App\Filters;



class UserFilters extends Filters
{

     protected $filters = ['name', 'blocked', 'limit', 'pos', 'mobile', 'card_number', 'sos', 'email', 'orderBy', 'count', 'with', 'filterByCategory', 'filterByCardType'];


     public function name($val)
     {

          return $this->builder->where('fullname', 'like', "%{$val}%");
     }
     public function email($val)
     {

          return $this->builder->where('email', 'like', "%{$val}%");
     }
     public function limit($val)
     {

          return $this->builder->limit($val);
     }

     public function pos($val)
     {

          if ($val == "false") return $this->builder;

          return $this->builder->where('type', 'pos');
     }
     public function blocked($val)
     {

          if ($val == "false") return $this->builder;

          return $this->builder->where('blocked_to', '!=', null);
     }

     public function mobile($val)
     {

          return $this->builder->where('mobile',  $val)->orWhere('mobile2', $val);
     }
     public function card_number($val)
     {

          return $this->builder->whereHas('cards', function ($query) use ($val) {
               return $query->withTrashed()->where('cards.number',  $val);
          });
     }

     public function filterByCategory($val)
     {


          return $this->builder->whereHas('categories', function ($query) use ($val) {
               return $query->whereIn('categories.id', explode(',', $val));
          });
     }
     public function orderBy($val)
     {
          list($col, $order) = explode(',', $val);


          return $this->builder->orderBy($col, $order);
     }

     public function filterByCardType($val)
     {

          if ($val === 'all') return $this->builder;

          return $this->builder->whereHas('cards', function ($query) use ($val) {

               return $query->whereIn('cards.card_type_id', explode(',', $val));
          });
     }
     public function count($val)
     {


          return $this->builder->withCount(explode(',', $val));
     }

     public function sos()
     {

          return $this->builder->where('sos', true);
     }
     public function with($val)
     {


          return $this->builder->with(['cards' => function ($query) {
               return $query->with(['package', 'cardType']);
          }])->with(explode(',', $val));
     }
}
