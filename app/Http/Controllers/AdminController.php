<?php

namespace App\Http\Controllers;

use App\Card;
use App\Filters\UserFilters;
use App\Imports\imports\UsersImport;
use App\Imports\imports\UsersSheetsImport;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Response;

use Illuminate\Support\Facades\File;

use Dilab\Network\SimpleRequest;
use Dilab\Network\SimpleResponse;
use Dilab\Resumable;

class AdminController extends Controller
{
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index(UserFilters $filters)
     {


          $results = User::admins()->latest()->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
          //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

          $this->validate($request, [
               'fullname' => 'required',
               'email' => 'required',
               'password' => 'required'
          ]);

          User::create([
               'type' => 'admin',
               'fullname' => request('fullname'),
               'email' => request('email'),
               'password' => Hash::make(request('password')),
          ]);
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request)
     {

          $this->validate($request, [
               'email' => 'required|email',
               'fullname' => 'required',
               'mobile' => 'required|phone:AUTO,EG'
          ]);

          $user = User::where('mobile', $request->input('mobile'))->orWhere('email', $request->input('email'))->first();



          if ($user && !auth()->user()->is($user)) {
               return response()->json(['errors' => ['wrong' => 'Mobile / Email Already Exists , try login']], 422);
          }


          auth()->user()->update([
               'type' => 'user',
               'fullname' => request('fullname'),
               'email' => request('email'),
               'mobile' => request('mobile')
          ]);

          $this->sendOtp(request(), auth()->user());
     }

     public function unlock()
     {

          $user = User::findOrFail(request('id'));

          $user->update([
               'uuid' => null
          ]);
     }
     public function sendOtp(Request $request, $user = null, $msg = 'Your BBM code is:')
     {





          $otp = rand(1000, 9000);

          $endpoint = "https://smsmisr.com/api/webapi/";
          $client = new \GuzzleHttp\Client();


          if ($request->has('hash') && $request->input('hash') != "") {

               $msg = "<#> " . $msg . ' ' . $otp . "\n"  . $request->input('hash');
          } else {
               $msg = "<#> " .  $msg . ' ' . $otp;
          }
          $response = $client->request('POST', $endpoint, ['query' => [
               'username' => 'QC2fV8qL',
               'password' => '6GuMA7unyI',
               'language' => 1,
               'Mobile' => $user ? $user->mobile : auth()->user()->mobile,
               'sender' => 'Benefits',
               'message' =>  $msg

          ]]);



          $user ? $user->update(['otp' => $otp]) : auth()->user()->update(['otp' => $otp]);
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
          User::destroy($id);
     }

     public function import()
     {




          Excel::import(new UsersSheetsImport(),  request()->file('file'));




          //    $tmpPath    = storage_path().'/tmp';
          //    $uploadPath = storage_path().'/uploads';
          //    if(!File::exists($tmpPath)) {
          //        File::makeDirectory($tmpPath, $mode = 0777, true, true);
          //    }

          //    if(!File::exists($uploadPath)) {
          //        File::makeDirectory($uploadPath, $mode = 0777, true, true);
          //    }

          //    $simpleRequest              = new SimpleRequest();
          //    $simpleResponse             = new SimpleResponse();

          //    $resumable                  = new Resumable($simpleRequest, $simpleResponse);
          //    $resumable->tempFolder      = $tmpPath;
          //    $resumable->uploadFolder    = $uploadPath;


          //    $result = $resumable->process();

          //    switch($result) {
          //        case 200:
          //            return response([
          //                'message' => 'OK',
          //            ], 200);
          //            break;
          //        case 201:






          //            return response([
          //                'message' => 'OK',
          //            ], 200);
          //            break;
          //        case 204:
          //            return response([
          //                'message' => 'Chunk not found',
          //            ], 204);
          //            break;
          //        default:
          //            return response([
          //                'message' => 'An error occurred',
          //            ], 404);
          //    }


     }
}
