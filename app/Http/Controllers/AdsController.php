<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Extras\FCMProvider;
use App\Notifications\Ad;
use App\Offer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdsController extends Controller
{


    public function test(FCMProvider $fcm)
    {

        $ad = Ads::select(['id', 'content'])->inRandomOrder()->limit(1)->first();


        $users = User::select(['id', 'token'])->get();


        $offer = Offer::first();

        $fcm->title('BBM')
            ->body('')
            ->data(['id' => $ad->id, 'content' => Str::limit($ad->content, 15)], 'ad')
            ->send($users->pluck('token')->toArray());


        $fcm->title('New Offer')
            ->body($offer->description)
            ->data(['id' => $offer->id], 'offer')
            ->send($users->pluck('token')->toArray());
    }

    public function index()
    {


        return Ads::latest()->paginate(request('paginate'));
    }


    public function get()
    {

        return Ads::whereHas('times', function ($query) {
            return $query->whereDate('broadcast_at', \Carbon\Carbon::now());
        })->random();
    }


    public function broadcast()
    {
    }


    public function store(FCMProvider $fcm)
    {

        $ad = Ads::create([
            'content' => request('content'),
            'title' => request('title'),
            'cover' => request('cover'),
            'cover_type' => request('cover_type'),
            'crons' => request('crons'),
            'car_types' => request('car_types')
        ]);



        if (request('sendNow') === true) {

            if (request()->filled('car_types') && sizeof(request('car_types'))) {

                $users = User::select(['id', 'token'])->whereIn('car_type', request('car_types'))->get();
            } else {
                $users = User::select(['id', 'token'])->where('token', '!=', null)->get();
            }


            $users->each(function ($user) use ($ad) {
                $user->notify(new Ad($ad));
            });



            $fcm->title($ad->title && !empty($ad->title)? $ad->title : 'BBM')
                ->body(Str::limit($ad->content, 15))
                ->data(['id' => $ad->id,'title'=>$ad->title, 'content' => Str::limit($ad->content, 15)], 'ad')
                ->send($users->pluck('token')->toArray());
        }


        return $ad;
    }

    public function update($id)
    {
         
         Ads::where('id' , $id)->update([
            'content' => request('content'),
            'title' => request('title'),
            'cover' => request('cover'),
            'cover_type' => request('cover_type'),
            'crons' => request('crons'),
            'car_types' => request('car_types')
        ]);
    }

    public function show($id)
    {

        return Ads::findOrFail($id);
    }

    public function destroy($id)
    {
         
Ads::destroy($id);
    }
}
