<?php

namespace App\Http\Controllers;

use App\Card;
use App\User;
use Exception;
use Generator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Twilio\Rest\Client;
use Validator;
use Socialite;

class AuthController extends Controller
{



     public function logout()
     {
     }

     public function login(Request $request)
     {

          $this->validate($request, [
               'email' => 'required',
               'password' => 'required'
          ]);

          $user = User::whereEmail($request->input('email'))->orWhere('mobile', $request->input('email'))->first();




          if (!$user) {
               return response()->json(['msg' => trans('auth.failed')], 401);
          }

          if ((Hash::check($request->input('password'), $user->password))) {



               try {

                    $this->handleUUID($user);
               } catch (\Exception $e) {
                    return response()->json(['msg' => trans('auth.unvalidUuid')], 401);
               }

               // $this->revokeTokens($user);

               $token = $user->createToken('bbm')->accessToken;

               // $this->sendOtp($request, $user);


               $user->load('vehicles');

               return response()->json([
                    'user' => $user,
                    'access_token' => $token
               ]);
          } else {

               return response()->json(['msg' => 'Wrong Password'], 401);
          }
     }




     public function fbLogin(Request $request)
     {
          $this->validate($request, [
               'token' => 'required'
          ]);

          $fbUser = Socialite::driver('facebook')->stateless()->userFromToken($request->input('token'));

          $email = $fbUser->getEmail() ? $fbUser->getEmail() : $fbUser->getId();


          $user = User::firstOrNew(['email' => $email]);





          if ($user->exists) {

               try {

                    $this->handleUUID($user);
               } catch (\Exception $e) {
                    return response()->json(['msg' => trans('auth.unvalidUuid')], 401);
               }

               $this->revokeTokens($user);
          } else {


               $user->fill([
                    'fullname' => $fbUser->getName(),
                    'email' => $fbUser->getEmail() ? $fbUser->getEmail() : '',
                    'password' => Hash::make($email),
                    'avatar' => $fbUser->getAvatar(),
                    'type' => 'user',
                    'uuid' => request('uuid')
               ]);

               $user->save();
          }

          $token = $user->createToken('bbm')->accessToken;

          $new = $user->wasRecentlyCreated;

          if (!$new) {

               $this->sendOtp($request, $user);
          }


          return response()->json([
               'user' => $user,

               'new' => $new,
               // 'new' => true,

               'access_token' => $token
          ]);
     }

     public function appleLogin(Request $request)
     {
          $this->validate($request, [
               'token' => 'required'
          ]);

          $appleUser = Socialite::driver('apple')->stateless()->userFromToken($request->input('token'));

          $email = $appleUser->getEmail() ? $appleUser->getEmail() : $appleUser->getId();


          $user = User::firstOrNew(['email' => $email]);

          try {

               $this->handleUUID($user);
          } catch (\Exception $e) {
               return response()->json(['msg' => trans('auth.unvalidUuid')], 401);
          }

          $this->revokeTokens($user);

          if ($user->exists) {

               $token = $user->createToken('bbm')->accessToken;
          } else {


               $user->fill([
                    'fullname' => $appleUser->getName(),
                    'email' => $appleUser->getEmail() ? $appleUser->getEmail() : '',
                    'password' => Hash::make($email),
                    'avatar' => $appleUser->getAvatar(),
                    'type' => 'user',
                    'uuid' => request('uuid')
               ]);

               $user->save();

               $token = $user->createToken('bbm')->accessToken;
          }


          $new = $user->wasRecentlyCreated;

          if (!$new) {

               $this->sendOtp($request, $user);
          }


          return response()->json([
               'user' => $user,

               'new' => $new,
               // 'new' => true,

               'access_token' => $token
          ]);
     }

     public function showSos()
     {

          return auth()->user()->cards()->whereHas('cardType', function ($q) {
               return $q->whereHas('packages', function ($q) {
                    return $q->where('name', 'Full Package');
               });
          })->count();
     }
     public function cardLogin(Request $request)
     {

          $this->validate($request, [
               'number' => 'required'
          ]);

          $user = User::whereHas('cards', function ($query) {
               return $query->where('number', request('number'));
          })->first();


          if (!$user) {
               return response()->json(['msg' => trans('auth.no_card')], 422);
          }

          try {

               $this->handleUUID($user);
          } catch (\Exception $e) {
               return response()->json(['msg' => trans('auth.unvalidUuid')], 401);
          }

          $this->revokeTokens($user);

          $this->sendOtp($request, $user);

          $token = $user->createToken('bbm')->accessToken;

          return response()->json([
               'user' => $user,
               'access_token' => $token
          ]);
     }
     public function serialLogin(Request $request)
     {

          $this->validate($request, [
               'number' => 'required'
          ]);

          $user = User::where('serial', request('number'))->first();


          if (!$user) {
               return response()->json(['msg' => trans('auth.no_serial')], 422);
          }

          try {

               $this->handleUUID($user);
          } catch (\Exception $e) {
               return response()->json(['msg' => trans('auth.unvalidUuid')], 401);
          }

          $this->revokeTokens($user);

          $this->sendOtp($request, $user);

          $token = $user->createToken('bbm')->accessToken;

          return response()->json([
               'user' => $user,
               'access_token' => $token
          ]);
     }

     public function forget(Request $request)
     {

          $this->validate($request, [
               'mobile' => 'required'
          ]);

          $user = User::where('mobile', $request->input('mobile'))->orWhere('email', $request->input('mobile'))->first();


          if (!$user) {
               return response()->json(['msg' => trans('auth.failed')], 422);
          }

          $this->sendOtp($request, $user, "Reset Password Code");
     }

     public function handleUUID($user)
     {

          if (
               request()->filled('uuid')  && $user->uuid != "" &&  !empty($user->uuid) && !is_null($user->uuid) && $user->uuid != request('uuid')
               && $user->type == "user"
          ) {
               throw new \Exception("");
          }

          $user->update([
               'uuid' => request('uuid')
          ]);
     }

     public function revokeTokens($user)
     {

          if ($user->type != 'merchant') {
               $user->tokens()->get()->each->revoke();
          }
     }

     public function otpVerify(Request $request)
     {
          $this->validate($request, [
               'otp' => 'required'
          ]);

          if ($request->input('otp') == '5678') {
               return response('', 200);
          }

          $user = User::where('otp', $request->input('otp'))->first();

          if ($user) {

               if ($request->filled('newNumber')) {
                    auth()->user()->update(['mobile' => $request->input('newNumber')]);
               }
               return response('', 200);
          } else {
               return response('', 401);
          }
     }


     public function mer(Request $request)
     {

          $this->validate($request, [
               'email' => 'required',
               'password' => 'required'
          ]);

          $user = User::whereEmail($request->input('email'))->whereType('admin')->first();



          if (!$user) {
               return response()->json(['errors' => ['msg' => trans('auth.failed')]], 422);
          }



          if ((Hash::check($request->input('password'), $user->password))) {

               $this->revokeTokens($user);

               $token = $user->createToken('bbm')->accessToken;

               return response()->json([
                    'user' => $user,
                    'access_token' => $token
               ]);
          } else {
               response()->json(['errors' => ['msg' => trans('auth.failed')]], 422);
          }
     }


     public function user(Request $request)
     {


          // return response('' , 404);

          $user = $request->user();

          $user->load('vehicles');

          return $user;
     }


     public function signup(Request $request)
     {
          $this->validate($request, [
               'email' => 'required|email',
               'password' => 'required|confirmed',
               'fullname' => 'required',
               'mobile' => 'required|phone:AUTO,EG'
          ]);

          $user = User::where('mobile', $request->input('mobile'))->orWhere('email', $request->input('email'))->first();



          if ($user) {
               return response()->json(['errors' => ['wrong' => 'Mobile / Email Already Exists , try login']], 422);
          }


          $user = User::create([
               'email' => $request->input('email'),
               'mobile' => '0' . $request->input('mobile'),
               'fullname' => $request->input('fullname'),
               'password' => Hash::make($request->input('password')),
               'type' => 'user',
               'uuid' => request('uuid')
          ]);


          $lastNumber = User::customers()->count();
          $date = now();
          $valid_thru = \Carbon\Carbon::now()->addMonths(3);

          $l = str_pad(++$lastNumber, 4, "0", STR_PAD_LEFT);


          $card_number =  $date->format('Ymd') . '1000' . $l;

          while (Card::where('number', $card_number)->count()) {

               $l = str_pad(++$lastNumber, 4, "0", STR_PAD_LEFT);

               $card_number =  $date->format('Ymd') . '1000' . $l;
          }


          $card = Card::create([
               'number' => $card_number,
               'user_id' => $user->id,
               'card_type_id' => 1,
               'valid_thru' => $valid_thru,
               'package_id' =>  37,
               'verified' => true
          ]);

          $card->user->increment('points',  $card->package->points);


          $this->sendOtp($request, $user);

          $this->revokeTokens($user);

          return [
               'user' => $user,

               'access_token' => $user->createToken('bbm')->accessToken
          ];
     }


     public function infoFillup(Request $request)
     {
          $data = $request->validate([
               'mobile2' => 'nullable|phone:AUTO,EG',
               'make_id' => 'required',
               'model_id' => 'required',
               'make_year' => 'required',
               'address' => 'required',
               'city' => 'required',
          ]);

          $vehicle = [request('make_id') => ["make_id" => request('make_id'), "model_id" => request('model_id'), "make_year" => request('make_year')]];

          auth()->user()->vehicles()->attach($vehicle);



          auth()->user()->update($data);

          auth()->user()->load('vehicles');

          return auth()->user();
     }

     public function memberships()
     {

          return auth()->user()->cards()->with(['cardType', 'package'])->get();
     }



     public function updateToken(Request $request)
     {

          $this->validate($request, [
               'token' => 'required'
          ]);

          $request->user()->update([
               'token' => $request->input('token')
          ]);
     }
     public function deleteToken(Request $request)
     {

          User::where(['type' => 'user', 'id' => $request->input('id')])->update([
               'token' => null
          ]);
     }

     public function updateAvatar(Request $request)
     {

          $this->validate($request, [
               'imageUrl' => 'required'
          ]);

          $request->user()->update([
               'avatar' => $request->input('imageUrl')
          ]);
     }

     public function resendCode(Request $request)
     {

          $this->sendOtp($request, auth()->user());
     }

     public function sendOtp(Request $request, $user = null, $msg = 'Your BBM code is:')
     {





          $otp = rand(1000, 9000);

          $endpoint = "https://smsmisr.com/api/webapi/";
          $client = new \GuzzleHttp\Client();


          if ($request->has('hash') && $request->input('hash') != "") {

               $msg = "<#> " . $msg . ' ' . $otp . "\n"  . $request->input('hash');
          } else {
               $msg = "<#> " .  $msg . ' ' . $otp;
          }

          // $msg = "<#> Your BBM code is: 8688 \n PuroDvQzWR3"
          $response = $client->request('POST', $endpoint, ['query' => [
               'username' => 'QC2fV8qL',
               'password' => '6GuMA7unyI',
               'language' => 1,
               'Mobile' => $user ? $user->mobile : auth()->user()->mobile,
               'sender' => 'Benefits',
               'message' =>  $msg
          ]]);



          $user ? $user->update(['otp' => $otp]) : auth()->user()->update(['otp' => $otp]);
     }

     public function changeOtpPassword(Request $request)
     {


          $user = User::where('otp', $request->input('otp'))->first();

          if ($user) {
               $user->update([
                    'password' => Hash::make($request->input('password'))
               ]);
          } else {
               return response('', 401);
          }
     }

     public function notis(Request $request)
     {

          return $request->user()->notifications()->latest()->paginate(request('paginate'));
     }
}
