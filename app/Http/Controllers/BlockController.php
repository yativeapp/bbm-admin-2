<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::findOrFail(request('id'));

        $user->update([
            'blocked_to' => $user->is_blocked ? null :  now()->addWeek(1)
        ]);
    }

}
