<?php

namespace App\Http\Controllers;

use App\Card;
use App\CardtypePackage;
use App\Extras\FCMProvider;
use App\Filters\BookingFilters;
use App\Offer;
use App\OfferBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BookingController extends Controller
{
     public function index(BookingFilters $filters)
     {



          return OfferBooking::filter($filters)->get();
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request, FCMProvider $fcm)
     {

          $request->validate([
               'offer_id' => 'required',
               'booked_at' => 'required',
               'times' => 'required|array',
          ]);

          $date = \Carbon\Carbon::parse(request('booked_at'));

          $offer = Offer::findOrFail(request('offer_id'));

          $times = collect($request->times)->map(function ($time) use ($date) {

               [$hour, $minute] = explode(":", $time);


               return [
                    'offer_id' => request('offer_id'),
                    'user_id' => auth()->id(),
                    'booking_at' => $date->setTime($hour, $minute)->toDateTimeString(),
               ];
          });

          OfferBooking::where('offer_id', request('offer_id'))->where('user_id', auth()->id())->delete();

          OfferBooking::insert($times->toArray());

          $fcm->title('New Booking')
               ->data([], 'new-booking')
               ->body(auth()->user()->fullname . ' Booked your offer')
               ->send($offer->merchant->token);
     }


     public function userBookings()
     {



          return auth()->user()->bookings()->with('offer')->get()->groupBy('offer_id')->values();
     }

     public function destroy($offer_id)
     {

          if (!$offer_id) abort(422);

          OfferBooking::where('user_id', auth()->id())
               ->where('offer_id', $offer_id)->delete();
     }
}
