<?php

namespace App\Http\Controllers;

use App\CardType;
use App\CarType;
use App\Filters\MerchantFilters;
use App\User;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    


     public function index(MerchantFilters $filters)
     {

          if(request()->filled('filterByCardType') && request('filterByCardType') != 'all'){
               if(request('filterByCardType') === "1"){

                    $collection = CarType::when(request('name') != '', function ($q) {
                         return $q->where('name', 'like', "%" . request('name') ."%");
                     })->get();
               }else{
                    $collection = User::merchants()->filter($filters)->get()->map(function($m){
                   
                         $m->icon = $m->avatar;
          
                         $m->name = $m->fullname;
          
                         return $m;
                    });
                   
               }
          }else{
               

               $carTypes = CarType::when(request('name') != '', function ($q) {
                    return $q->where('name', 'like', "%" . request('name') ."%");
                })->get();

               $merchantBrands = User::merchants()->filter($filters)->get()->map(function($m){
                   
                    $m->icon = $m->avatar;
     
                    $m->name = $m->fullname;
     
                    return $m;
               });


               $collection = $carTypes->merge($merchantBrands);
          }

          
           

      

          return $collection->sortBy(function ($brand) {
               
               return strtolower($brand->name[0]);
           })->values();
     
     }
}