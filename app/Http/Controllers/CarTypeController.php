<?php

namespace App\Http\Controllers;

use App\CarType;
use App\Filters\CarTypeFilters;
use Illuminate\Http\Request;

class CarTypeController extends Controller
{


    public function index(CarTypeFilters $filters)
    {
        $results = CarType::filter($filters);

        if (request()->has('paginate') && request('paginate')) {
            return $results->paginate(intVal(request('paginate')));
        }

        return $results->get();
    }


    public function store()
    {
         
        return CarType::create([
            'name' => request('name'),
            'icon' => request('icon'),
        ]);
    }

    public function update($id)
    {
         
        CarType::where('id' , $id)->update([
            'name' => request('name'),
            'icon' => request('icon'),
        ]);
    }
}
