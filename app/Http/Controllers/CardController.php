<?php

namespace App\Http\Controllers;

use App\Card;
use App\CardRequest;
use App\CardType;
use App\CardtypePackage;
use App\CarType;
use App\Extras\FCMProvider;
use App\Filters\CardFilters;
use App\Notifications\RequestAccepted;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Plansky\CreditCard\Generator;

use Plansky\CreditCard\Validator;

class CardController extends Controller
{

     public function index(CardFilters $filters)
     {

          $results = Card::whereHas('cardType', function ($q) {
               return $q->where('type', 'normal');
          })->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(intVal(request('paginate')));
          }

          return $results->get();
     }

     public function store(Request $request, FCMProvider $fcm)
     {





          $number = null;

          $date = now();

          if (request()->has('auto_generate_number') && request('auto_generate_number')) {


               $lastSerial = Card::withTrashed()->count();



               $l = str_pad(++$lastSerial, 4, "0", STR_PAD_LEFT);


               $number =  $date->format('Ymd') . '1000' . $l;

               while (Card::where('number', $number)->count()) {

                    $l = str_pad(++$lastSerial, 4, "0", STR_PAD_LEFT);


                    $number =  $date->format('Ymd') . '1000' . $l;
               }
          } else {


               if ($request->has('card_number') && request()->filled('card_number')) {

                    if ($request->has('custom_number') && $request->custom_number) {

                         $number = $request->card_number;
                    } else {

                         $number =  $date->format('Ymd') . '1000' . $request->card_number;
                    }

                    $exsist = Card::where('number', $number)->count();

                    if ($exsist) {

                         return response()->json([
                              'This Card Number Already exsist'
                         ], 500);
                    }
               }
          }

          if (!$number) {
               return response()->json([
                    'Please Add Card Number'
               ], 500);
          }


          $valid_thru = $request->filled('valid_thru') ? \Carbon\Carbon::parse($request->valid_thru)->addDay() : now()->addMonths(2);

          if ($valid_thru->isPast()) {
               return response()->json([
                    'Valid Thru is In The Past'
               ], 500);
          }


          $user_id = request('user_id');
          $card_type_id = request('card_type_id');
          $package_id = request('package_id');

          if ($request->has('request_id')) {

               $cardRequest = CardRequest::find(request('request_id'));

               if (!$cardRequest) {
                    return response()->json([
                         'Invalid Request'
                    ], 500);
               }


               if (request()->filled('pos') && request('pos')) {

                    $cardRequest->update([
                         'moved_to_pos_at' => now(),
                         'package_id' => $package_id,
                         'valid_thru' => $valid_thru
                    ]);

                    return;
               }

               $cardRequest->load(['user', 'cardType']);

               $user_id = $cardRequest->user_id;
               $card_type_id = $cardRequest->card_type_id;


               $title = "Accepted ✅";

               $body = "Your Request to " . $cardRequest->cardType->name . " Membership have been accepted";

               $fcm->title($title)
                    ->body($body)
                    ->data(['title' => $title, 'body' => $body], 'request-accepted')
                    ->send($cardRequest->user->token);



               // $cardRequest->user->notify(new RequestAccepted($title, $body));

               $cardRequest->delete();
          }



          $card = Card::create([
               'number' => $number,
               'user_id' => $user_id,
               'card_type_id' => $card_type_id,
               'package_id' => $package_id,
               'valid_thru' => $valid_thru,
               'verified' => true,
               'pos_agent' => $request->filled('issued_from_pos') &&  $request->issued_from_pos ? auth()->id() : null
          ]);




          $card->user->increment('points',  $card->package->points);


          if ($request->has('request_id')) {
               $card->user->update([
                    'address' => $cardRequest->address,
                    'id_no' => $cardRequest->national_number
               ]);

               $carType = CarType::where('name', $cardRequest->car_type)->first();

               if ($carType) {
                    $card->user->carTypes()->attach($carType->id);
               }

              
          }

          $card->load(['cardType', 'user']);

          return $card;
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {

          Card::destroy($id);
     }

     public function total()
     {

          return Card::count();
     }


     public function update($id)
     {

          request()->validate([
               'valid_thru' => 'required'
          ]);

          $card = Card::withTrashed()->with('package')->find($id);

          $data = [];





          $valid_thru = \Carbon\Carbon::parse(request('valid_thru'))->addDay();


          if ($card->valid_thru->eq($valid_thru) && request('package_id') == $card->package_id) {
               return 'same';
          }


          $data['valid_thru'] = $valid_thru;

          $data['package_id'] = request('package_id');

          $isPast = $valid_thru->isPast();


          if ($isPast) {
               $card->update($data);
               $card->deleted_at ? null : $card->delete();

               return 'is_past';
          }

          $card->restore();





          if ($card->package_id != request('package_id')) {

               $pointsUsed = $card->user->sales->whereBetween('created_at', [now(), $card->created_at])->count();

               $pointsToDeduct = ($card->package->points - $pointsUsed) < 0 ? 0 : $card->package->points - $pointsUsed;

               $points = ($card->user->points - $pointsToDeduct) < 0 ? 0 : $pointsToDeduct;

               $card->user->decrement('points', $points);


               $package = CardtypePackage::find(request('package_id'));

               $card->user->increment('points', $package->points);
          } else {

               $card->user->increment('points', $card->package->points);
          }

          $card->update($data);
     }

     public function overmonths()
     {
          $sc = Card::whereYear('created_at', Carbon::now()->year)->get();

          $months = [];


          for ($m = 1; $m <= 12; ++$m) {
               $months[date('F', mktime(0, 0, 0, $m, 1))] = 0;
          }


          $sc = $sc->groupBy(function ($sc) {
               return $sc->created_at->format('F');
          })->map(function ($sc) {
               return $sc->count();
          });

          return collect($months)->merge($sc)->values();
     }
}
