<?php

namespace App\Http\Controllers;

use App\Card;
use App\CardType;
use App\Filters\CardTypeFilters;
use App\Filters\OfferFilters;
use App\Offer;
use Plansky\CreditCard\Generator;
use Illuminate\Http\Request;

class CardTypeController extends Controller
{

     public function index(CardTypeFilters $filters)
     {

          $results = CardType::filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function gifts(CardTypeFilters $filters)
     {



          $results = CardType::where('type', 'gift')->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function claimGift(Generator $generator)
     {




          $gift = CardType::with('packages')->findOrFail(request('id'));

          $before = Card::where(['user_id' => auth()->id(), 'card_type_id' => $gift->id])->count();


          if ($before) {
               return response()->json(['msg' => 'used_before'], 500);
          }


          $paid_user = auth()->user()->cards()->whereHas('cardType', function ($query) {
               return $query->where('card_types.type', 'normal');
          })->count();


          if (!$paid_user) {

               $claimed_gift_before = auth()->user()->cards()->whereHas('cardType', function ($query) {
                    return $query->where('card_types.type', 'gift');
               })->count();


               if ($claimed_gift_before) {
                    return response()->json(['msg' => 'cant_claim'], 500);
               }
          }



          $package_id = $gift->packages->first()->id;

          Card::create([
               'number' => $generator->single(),
               'user_id' => auth()->id(),
               'card_type_id' => $gift->id,
               'package_id' => $package_id,
               'valid_thru' => \Carbon\Carbon::now()->addMonth(3),
               'verified' => true
          ]);




          auth()->user()->increment('points',  $gift->packages ? $gift->packages->first()->points : 50);

          return $gift;
     }

     public function show($id)
     {
          return CardType::findOrFail($id);
     }

     public function verifyCard()
     {

          $card = auth()->user()->cards()->where('number', request('number'))->first();

          if ($card) {

               if ($card->verified) {
                    return response()->json(['wrong' => 'Already Activated'], 422);
               }
               $card->update(['verified' => true]);

               $card->load(['cardType', 'package']);

               auth()->user()->increment('points', $card->package->points);

               return $card;
          }

          return response()->json(['wrong' => 'Wrong Card Number'], 422);
     }

     public function verifyAll()
     {

          Card::with(['user', 'package'])->where('verified' , false)->chunkById(100, function ($cards){


               foreach ($cards as $card) {


                    $card->update(['verified' => true]);

                    $card->load(['cardType']);

                    $card->user->increment('points', $card->package->points);
               }
          });

     }

     public function userGifts()
     {

          return auth()->user()->cards()->with('cardType.allOffers')->whereHas('cardType', function ($q) {
               return $q->where('type', 'gift');
          })->get();
     }
     public function userCoupons(OfferFilters $filters)
     {

          $giftCards = auth()->user()->cards()->with('cardType')->get();


          $x = $giftCards->pluck('cardType')->pluck('id');

          return Offer::filter($filters)->with('cardTypes')->has('cardTypes')->inRandomOrder()->limit(300)->whereHas('discounts', function ($q) use ($giftCards) {
               return $q->whereIn('package_id', $giftCards->pluck('package_id'));
          })->get()->groupBy(function ($offer) use ($x) {


               $y = $offer->cardTypes->pluck('id')->intersect($x)->first();


               $ty = $offer->cardTypes->where('id', $y)->first();

               if ($ty) {
                    return $ty->name;
               } else {
                    $offer->cardTypes->first()->name;
               }
          });
     }

     public function userCoupons2(OfferFilters $filters)
     {

          $giftCards = auth()->user()->cards()->with('cardType')->get();

          $results  = Offer::filter($filters)->has('cardTypes')->inRandomOrder()->whereHas('discounts', function ($q) use ($giftCards) {
               return $q->whereIn('package_id', $giftCards->pluck('package_id'));
          });

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function store(Request $request)
     {



          $this->validate($request, [
               'name' => 'required',

          ]);


          $card =  CardType::create([
               'name' => $request->name,
               'type' => $request->has('gift') && $request->input('gift') ? 'gift' : 'normal',
               'description' => $request->description,
               'cover' => $request->cover,
               'logo' => $request->logo,
               'bgColor' => $request->bgColor,
               'for' => $request->target ? $request->target : 'paid_users'
          ]);

          if ($request->has('gift') && $request->input('gift')) {
               $card->packages()->create([
                    'points' => $request->points,
                    'name' => 'default'
               ]);
          }

          return $card;
     }

     public function patch(Request $request, $id)
     {

          $this->validate($request, [
               'name' => 'required',
               'description' => 'required',
          ]);

          $data  = [
               'name' => request('name'),
               'description' => request('description'),
               'bgColor' => request('bgColor'),
          ];

          if (request()->filled('cover')) {
               $data['cover'] = request('cover');
          }
          if (request()->filled('logo')) {
               $data['logo'] = request('logo');
          }

          return CardType::where('id', $id)->update($data);
     }

     public function destroy($id)
     {


          CardType::destroy($id);
     }
}
