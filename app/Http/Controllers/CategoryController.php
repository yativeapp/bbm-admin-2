<?php

namespace App\Http\Controllers;

use App\Category;
use App\Filters\CategoryFilters;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

     public function index(CategoryFilters $filters)
     {

          $results = Category::filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function show($id)
     {
          return Category::findOrFail($id);
     }

     public function merchants($id)
     {
          $category = Category::findOrFail($id);

          return $category->merchants()->paginate(10);
     }

     public function store(Request $request)
     {



          $this->validate($request, [
               'name' => 'required',
               'icon' => 'required',
               'card_type' => 'required'
          ]);


          $cat = Category::create([
               'name' => $request->name,
               'icon' => $request->icon,
               'icon_type' => $request->icon_type
          ]);

          $cat->cardTypes()->attach(request('card_type'));

          return $cat;
     }

     public function patch(Request $request, $id)
     {

          $this->validate($request, [
               'name' => 'required',
               'icon' => 'required'

          ]);


          return Category::where('id', $id)->update($request->only(['name', 'icon', 'icon_type']));
     }

     public function destroy($id)
     {


          Category::destroy($id);
     }
}
