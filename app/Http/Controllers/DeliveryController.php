<?php

namespace App\Http\Controllers;

use App\Extras\FCMProvider;
use App\Filters\DeliveryFilters;
use App\Offer;
use App\OfferDelivery;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    
    public function index(DeliveryFilters $filters)
    {
        $results = OfferDelivery::filter($filters);

        if (request()->has('paginate') && request('paginate')) {
            return $results->paginate(request('paginate'));
        }

        return $results->latest()->get();
    }

    public function store(FCMProvider $fcm)
    {

        $offer = Offer::find(request('offer_id'));
         
        $order = OfferDelivery::create([
            'user_id' => auth()->id(),
            'merchant_id' => $offer->merchant_id,
            'offer_id' => $offer->id,
            'address' => request('address'),
            'lat' => request('lat'),
            'lng' => request('lng')
        ]);



        $fcm->title('New Order')
        ->body("New Order For -" . $offer->title . "- Offer")
            ->data([], 'new-order')
            ->send([$order->merchant->token]);
    }


    public function changeStatus(FCMProvider $fcm)
    {
         
       $order =  OfferDelivery::findOrFail(request('id'));

       
       
       $order->update([
            'status' => request('status')
        ]);


        $fcm->title('Order ' . ucwords($order->status))
        ->body("Your Order For -" . $order->offer->title . "- status changed")
        ->data([], 'order-status-changed')
        ->send([$order->user->token]);
    }

    public function destroy($id)
    {
        OfferDelivery::destroy($id);
    }
}
