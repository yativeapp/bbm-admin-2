<?php

namespace App\Http\Controllers;

use App\Card;
use App\CardType;
use App\MerchantBranch;
use App\Offer;
use App\OfferOpenings;
use App\Sale;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GeoController extends Controller
{

     public function get(Request $request)
     {


          return  MerchantBranch::distance($request->lat, $request->lng)->having('distance', '<=', 2)->get();
     }

     public function canRedeem(Request $request)
     {

          $user = User::find(auth()->id());



          if ($user->is_blocked) return $this->res(false, 'blocked');


          if ($user->points === 0) return $this->res(false, 'not_enough_points');


          $offer = Offer::with(['cardTypes', 'discounts'])->find(request('offer_id'));

          if (request()->filled('gift_card_id') && !$offer->allTime) {

               $giftCard = Card::find(request('gift_card_id'));

               $allowedDays  = $offer->end_date->diffInDays($offer->start_date);

               $span = $giftCard->created_at->addDays($allowedDays);

               if (now()->gt($span)) {
                    return $this->res(false, 'expired');
               }
          } else {

               if ($offer->expired && !$offer->allTime) return $this->res(false, 'expired');
          }



          $of = $offer->discounts->pluck('package_id');


          $cards = $user->cards()->with('cardType')->get();

          $userCardTypes = $cards->pluck('package_id');

          if ($of->intersect($userCardTypes)->count()) {
          } else {

               $offer->discounts->load(['cardType', 'package']);

               return $this->res(false, 'not_in_membership', $offer->discounts);
          }


          // 1. used before

          $used_before = Sale::where('user_id', auth()->id())->where('offer_id', $request->offer_id)->get();

          if ($used_before->count()) {
               return $this->res(false, 'used_before', ['at' => $used_before->first()->created_at->diffForHumans()]);
          }



          // 2. distance

          $x = MerchantBranch::distance($request->lat, $request->lng)->having('distance', '<=', 2)
               ->whereHas('offers', function ($q) {
                    return $q->where('offers.id', request('offer_id'));
               })
               ->where('merchant_id', request('merchant_id'))->get();


          if ($x->count()) {

               // 3. timed out

               $offerOpen = OfferOpenings::firstOrCreate([
                    'user_id' => auth()->id(),
                    'offer_id' => $request->offer_id
               ]);

               if ($offerOpen->wasRecentlyCreated === false && now()->diffInHours($offerOpen->created_at) >= 3) {

                    return $this->res(false, "timedout");
               }
          } else {

               $offer->load('branches');

               return $this->res(false, 'far_away', ['offer' => $offer]);
          }


          return $this->res(true);
     }


     private function res($can, $reason = "", $data = [])
     {
          return response()->json(['can' => $can, 'reason' => $reason, 'data' => $data]);
     }
}
