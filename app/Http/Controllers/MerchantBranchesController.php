<?php

namespace App\Http\Controllers;

use App\Category;
use App\Filters\BranchFilters;
use App\MerchantBranch;
use Illuminate\Http\Request;

class MerchantBranchesController extends Controller
{

     public function index(BranchFilters $filters)
     {

          $results = MerchantBranch::filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function show($id)
     {
          return Category::findOrFail($id);
     }

     public function merchants($id)
     {
          $category = Category::findOrFail($id);

          return $category->merchants()->paginate(10);
     }

     public function autocomplete()
     {
          $val = request('name', '');

          return MerchantBranch::withOut('merchant')->where('name', 'like', "%{$val}%")
               ->orWhere('address_title', 'like', "%{$val}%")
               ->paginate(30);
     }

     public function store(Request $request)
     {



          $this->validate($request, [
               'name' => 'required',
               'lat' => 'required',
               'lng' => 'required',
               'mobile' => 'required',
               'address_title' => 'required',
               'merchant_id' => 'required'
          ]);

          return MerchantBranch::create($request->all());
     }

     public function update($id)
     {

          request()->validate([
               'name' => 'required',
               'lat' => 'required',
               'lng' => 'required',
               'address_title' => 'required',
               'merchant_id' => 'required'
          ]);


          MerchantBranch::where('id', $id)->update(
               request()->only([
                    'name',
                    'mobile',
                    'lat',
                    'lng',
                    'merchant_id',
                    'address_title'
               ])
          );
     }

     public function destroy($id)
     {


          MerchantBranch::destroy($id);
     }
}
