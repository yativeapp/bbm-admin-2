<?php

namespace App\Http\Controllers;

use App\CardtypePackage;
use App\CarType;
use App\Category;
use App\Exports\MerchantsExport;
use App\Extras\FCMProvider;
use App\Filters\MerchantFilters;
use App\Filters\OfferFilters;
use App\Imports\imports\MercahntsImport;
use App\Imports\imports\MerchantSheetsImport;
use App\Imports\imports\UsersImport;
use App\MerchantBranch;
use App\Offer;
use App\OfferBooking;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;


use Illuminate\Support\Facades\File;

use Dilab\Network\SimpleRequest;
use Dilab\Network\SimpleResponse;
use Dilab\Resumable;

class MerchantController extends Controller
{

     public function index(MerchantFilters $filters)
     {



          $results = User::merchants()->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }
     public function index2(MerchantFilters $filters, OfferFilters $offerFilters)
     {

          $searchKey = request('name');

          if (!request()->filled('name')) {

               return $this->index($filters);
          }

          $merchants = User::merchants()->filter($filters)->limit(5)->latest()->get();
          $offers = Offer::filter($offerFilters)->limit(5)->latest()->get();

          return ['offers' => $offers, 'merchants' => $merchants];
     }


     public function show($id)
     {

          return User::findOrFail($id);
     }


     public function store(Request $request, FCMProvider $fcm)
     {

          //      $this->validate($request, [
          //           'card_type_id' => 'required',
          //           'points' => 'required'
          //      ]);


          //    return  CardtypePackage::create([
          //           'card_type_id' => $request->input('card_type_id'),
          //           'points' => $request->input('points'),
          //      ]);


          $this->validate($request, [
               'fullname' => 'required'
          ]);


          $serial = null;

          $date = now();

          if (request()->has('auto_generate_number') && request('auto_generate_number')) {


               $lastSerial = User::merchants()->count();



               $l = str_pad(++$lastSerial, 4, "0", STR_PAD_LEFT);


               $serial =  $date->format('Ymd') . '9000' . $l;

               while (User::merchants()->where('serial', $serial)->count()) {

                    $l = str_pad(++$lastSerial, 4, "0", STR_PAD_LEFT);

                    $serial =  $date->format('Ymd') . '9000' . $l;
               }
          } else {


               if ($request->has('serial')) {

                    if ($request->has('custom_number') && $request->custom_number) {

                         $serial = $request->serial;
                    } else {

                         $serial =  $date->format('Ymd') . '9000' . $request->serial;
                    }

                    $exsist = User::merchants()->where('serial', $serial)->count();

                    if ($exsist) {

                         return response()->json([
                              'This Serial Already exsist'
                         ], 500);
                    }
               }
          }


          $user  = User::create([
               'fullname' => $request->input('fullname'),
               'email' => $request->input('email'),
               'serial' => $serial,
               'avatar' => $request->input('avatar'),
               'job' => $request->input('job'),
               'password' => Hash::make('bbm_123'),
               'top' => $request->input('top'),
               'brand' => $request->input('brand'),
               'type' => 'merchant',
               'all_cars' => request()->has('all_cars')
          ]);


          if (request()->has('categories') && request('categories')) {
               $user->categories()->sync(explode(',', request('categories')));
          }

          if (request()->has('cards') && request('cards')) {
               $user->cardTypes()->sync(explode(',', request('cards')));
          }

          if (request()->has('carTypes') && request('carTypes')) {
               $user->carTypes()->sync(explode(',', request('carTypes')));
          }

          $user->times()->createMany(request('times', []));


          $users = User::select(['id', 'token'])->get();


          $fcm->title("New BBM Family Member : " . $user->fullname)
               ->body($user->fullname)
               ->data(['id' => $user->id], 'new-merchant')
               ->send($users->pluck('token')->toArray());


          return $user;
     }


     public function branches()
     {

          $results = MerchantBranch::where('merchant_id', auth()->id())->latest();

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }
     public function bookings()
     {

          $results = OfferBooking::with(['offer', 'user'])->whereHas('offer', function ($query) {
               $query->where('merchant_id', auth()->id());
          })->whereDate('booking_at', \Carbon\Carbon::parse(request('date')))->latest();


          return [
               'data' => $results->get()->groupBy('offer_id')->values()
          ];
     }

     public function patch(Request $request, $id)
     {


          $this->validate($request, [
               'fullname' => 'required'
          ]);

          $user = User::find($id);


          $user->update([
               'fullname' => $request->fullname,
               'email' => $request->email,
               'avatar' => $request->avatar,
               'top' => $request->input('top'),
               'brand' => $request->input('brand'),
               'all_cars' => request()->has('all_cars')
          ]);



          if (request()->has('categories') && request('categories')) {
               $user->categories()->sync(explode(',', request('categories')));
          }

          if (request()->has('cards') && request('cards')) {
               $user->cardTypes()->sync(explode(',', request('cards')));
          }

          if (request()->has('carTypes') && request('carTypes')) {
               $user->carTypes()->sync(explode(',', request('carTypes')));
          }

          $user->times()->delete();
          $user->times()->createMany(request('times', []));
     }

     public function toggleTop()
     {

          $user = User::findOrFail(request('id'));

          $user->update(['top' => request('top')]);
     }

     public function destroy($id)
     {


          User::destroy($id);
     }

     public function total()
     {

          return User::merchants()->count();
     }

     public function overmonths()
     {
          $sc = User::merchants()->whereYear('created_at', Carbon::now()->year)->get();

          $months = [];


          for ($m = 1; $m <= 12; ++$m) {
               $months[date('F', mktime(0, 0, 0, $m, 1))] = 0;
          }


          $sc = $sc->groupBy(function ($sc) {
               return $sc->created_at->format('F');
          })->map(function ($sc) {
               return $sc->count();
          });

          return collect($months)->merge($sc)->values();
     }


     public function import()
     {


          Excel::import(new MerchantSheetsImport(), request()->file('file'));
     }

     public function export()
     {
          return Excel::download(new MerchantsExport, 'merchants.xlsx');
     }
}
