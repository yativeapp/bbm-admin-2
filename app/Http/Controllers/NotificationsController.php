<?php

namespace App\Http\Controllers;

use App\Extras\FCMProvider;
use App\User;
use App\Notifications\Admin;
use App\Notifications\Offer as NotificationsOffer;
use App\Offer;

class NotificationsController extends Controller
{



     public function admin(FCMProvider $fcm)
     {

          $users = User::select(['id', 'token'])->where('type' , '!=' , 'pos')->where('token', '!=', null)->get();





          $fcm->title(request('title'))
               ->body(request('body'))
               ->data(['title' => request('title'), 'body' => request('body')], 'admin')
               ->send($users->pluck('token')->toArray());



          $users->each(function ($user) {
               $user->notify(new Admin(request('title'), request('body')));
          });
     }

     public function offer($id)
     {
          $fcm = new FCMProvider;

          $users = User::customers()->select(['id', 'token'])->get();


          $offer = Offer::with(['branches' , 'merchant'])->findOrFail($id);

          $branch = $offer->branches->first() ? $offer->branches->first()->name : "";

          $title = $offer->merchant->fullname . " -  " . $branch;

          $fcm->title($title)
               ->body($offer->description)
               ->data(['id' => $offer->id], 'offer')
               ->send($users->pluck('token')->toArray());


          $users->each(function ($user) use ($offer , $title) {
               $user->notify(new NotificationsOffer($offer , $title));
          });
     }

     public function sendSosOffers(FCMProvider $fcm)
     {

          $with = ['offers' => function ($q) {
               return $q->withoutGlobalScopes()->where('sos', true);
          } , 'offers.merchant'];

          $authCartypes = auth()->user()->carTypes->pluck('id')->toArray();



          return User::with($with)->merchants()->whereHas('carTypes', function ($query) use ($authCartypes) {
               return $query->whereIn('car_types.id', $authCartypes);
          })->orWhere('all_cars' , true)->get()
               ->pluck('offers')->flatten();

               
          User::with($with)->merchants()->whereHas('carTypes', function ($query) use ($authCartypes) {
               return $query->whereIn('car_types.id', $authCartypes);
          })->orWhere('all_cars' , true)->get()
               ->pluck('offers')->flatten()->each(function ($offer) use ($fcm) {

                    $user = User::find(auth()->id());

                    $c = $offer->branches->count();

               $branch = $c ? $offer->branches->first()->name : "";

               if($c){

                    $title = $offer->merchant->fullname . " -  " . $branch;
               }else{
                    $title = $offer->merchant->fullname;
               }

                    $fcm->title($title)
                         ->body($offer->description)
                         ->data(['id' => $offer->id], 'offer')
                         ->send([$user->token]);

                    $user->notify(new NotificationsOffer($offer , $title));
               });
     }
}
