<?php

namespace App\Http\Controllers;

use App\CardType;
use App\Extras\FCMProvider;
use App\Filters\OfferFilters;
use App\Offer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class OfferController extends Controller
{

     public function index(OfferFilters $filters)
     {

          // $userCards = auth()->user()->cards()->get();


          // $results = Offer::whereHas('discounts' , function($query) use ($userCards){
          //    return $query->whereIn('offer_discounts.package_id' , $userCards->pluck('package_id')->toArray());
          // })->filter($filters);


          $results = Offer::filter($filters);



          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     public function total()
     {

          return Offer::withoutGlobalScopes()->count();
     }

     public function show($id)
     {
          return Offer::withOutGlobalScopes()->findOrFail($id);
     }


     public function store(Request $request, FCMProvider $fcm)
     {



          $this->validate($request, [
               'title' => 'required',
               'description' => 'required',
               'merchant_id' => 'required|exists:users,id',
          ]);


          return Collection::times(1, function () use ($request) {
               $data = [

                    'title' => request('title'),
                    'description' => request('description'),
                    'cover' => request('cover'),
                    'allTime' => request('allTime'),
                    'merchant_id' => request('merchant_id'),
                    'sos' => request('sos'),
                    'deliverable' => request('deliverable'),
                    'bookable' => request('bookable'),
                    'service_time' => request('service_time'),
                    'user_per_time' => request('user_per_time'),
               ];


               if (!request('allTime')) {

                    $data['start_date'] = Carbon::parse(request('start_date'));
                    $data['end_date'] = Carbon::parse(request('end_date'));
               }

               $offer = Offer::create($data);




               collect($request->discounts)->each(function ($discount) use ($offer) {

                    $cardType = CardType::find($discount['card_type_id']);


                    $offer->cardTypes()->save($cardType);

                    collect($discount['packages_ids'])->each(function ($package) use ($offer, $discount) {




                         $offer->discounts()->create([
                              'discount' => $discount['discount'],
                              'discount_type' => $discount['discount_type'],
                              'card_type_id' => $discount['card_type_id'],
                              'package_id' => $package
                         ]);
                    });
               });


               $offer->branches()->attach(request('branch_ids'));

               return $offer;
          });
     }

     public function update(Request $request, $id)
     {

          $this->validate($request, [
               'title' => 'required',
               'description' => 'required'
          ]);

          $offer = Offer::withoutGlobalScopes()->find($id);



          $data = [

               'title' => request('title'),
               'description' => request('description'),
               'merchant_id' => request('merchant_id'),
               'allTime' => request('allTime'),
               'sos' => request('sos'),
               'deliverable' => request('deliverable'),
               'bookable' => request('bookable'),
               'service_time' => request('service_time'),
               'user_per_time' => request('user_per_time'),
          ];


          if (!request('allTime')) {

               $data['start_date'] = Carbon::parse(request('start_date'));
               $data['end_date'] = Carbon::parse(request('end_date'));
          }


          if (request()->filled('cover')) {
               $data['cover'] = request('cover');
          }

          $cardTypes = collect($request->discounts)->pluck('card_type_id')->toArray();

          $offer->cardTypes()->sync($cardTypes);

          $offer->discounts()->delete();

          collect($request->discounts)->each(function ($discount) use ($offer) {

               collect($discount['packages_ids'])->each(function ($package) use ($offer, $discount) {




                    $offer->discounts()->create([
                         'discount' => $discount['discount'],
                         'discount_type' => $discount['discount_type'],
                         'card_type_id' => $discount['card_type_id'],
                         'package_id' => $package
                    ]);
               });
          });


          $offer->update($data);

          $offer->branches()->sync(request('branch_ids'));

          $offer->load(['discounts', 'branches', 'cardTypes', 'merchant']);


          return $offer;
     }

     public function destroy($id)
     {


          Offer::withOutGlobalScopes()->find($id)->forceDelete($id);
     }
}
