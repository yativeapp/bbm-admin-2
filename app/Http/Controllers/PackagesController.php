<?php

namespace App\Http\Controllers;

use App\CardtypePackage;
use App\Filters\PackageFilters;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PackageFilters $filters)
    {
        $results = CardtypePackage::has('cardType')->latest()->filter($filters);

        if (request()->has('paginate') && request('paginate')) {
            return $results->paginate(request('paginate'));
        }

        return $results->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $p =  CardtypePackage::create([
            'name' => request('name'),
            'card_type_id' => request('card_type_id'),
            'points' => request('points'),
            'price' => request()->input('price', 0),
            'in_stock' => request()->input('in_stock', true)
        ]);

        $p->load('cardType');

        return $p;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function cardTypes()
    {
        return CardtypePackage::where('card_type_id', request('card_type_id'))->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $p = CardtypePackage::findOrFail($id);

        $p->update([
            'name' => request('name'),
            'points' => request('points'),
            'price' => request()->input('price', 0),
            'in_stock' => request()->input('in_stock', true)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        CardtypePackage::destroy($id);
    }
}
