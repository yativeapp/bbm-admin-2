<?php

namespace App\Http\Controllers;

use App\Card;
use App\Filters\PosFilters;

class PosController extends Controller
{


     public function index(PosFilters $filters)
     {

          $results = Card::pos()->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get(); 
     }
}