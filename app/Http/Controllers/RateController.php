<?php

namespace App\Http\Controllers;

use App\Sale;
use Illuminate\Http\Request;

class RateController extends Controller
{
    

    public function userRate()
    {
         
        $sale = Sale::findOrFail(request('sale_id'));

        $sale->update(['user_rate' => request('rate')]);
    }
    public function merchantRate()
    {
         
        $sale = Sale::findOrFail(request('sale_id'));

        $sale->update(['merchant_rate' => request('rate')]);
    }
}
