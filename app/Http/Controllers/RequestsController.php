<?php

namespace App\Http\Controllers;

use App\Filters\RequestFilters;
use App\CardRequest;
use App\CardtypePackage;
use App\Extras\Accept;

class RequestsController extends Controller
{
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index(RequestFilters $filters)
     {


          $results = CardRequest::has('cardType')->latest()->filter($filters);

          if (request()->has('paginate') && request('paginate')) {
               return $results->paginate(request('paginate'));
          }

          return $results->get();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
          //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store()
     {

          request()->validate([
               'card_type_id' => 'required'
          ]);

          $t = request('payment_method') == 'cod' ? 'delivery' : 'pos';

          return CardRequest::create([
               'card_type_id' => request('card_type_id'),
               'package_id' => request('package_id'),
               'car_type' => request('car_type'),
               'payment_method' => request('payment_method'),
               'address' => request('address'),
               'receive_type' => $t,
               'user_id' => auth()->id()
          ]);
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
          //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
          //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update($id)
     {
          //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {

          CardRequest::destroy($id);
     }


     public function pay()
     {

          $cardRequest = CardRequest::with('package')->findOrFail(request('id'));

          $a = new Accept;

          $a->charge($cardRequest->package ? intVal($cardRequest->package->price) : 100);

          $src = $a->getIFrame();

          return response()->json(['frame' => $src]);
     }


     public function updateDelivery()
     {
    
          $data = [
               'receive_type' => request('receive_type')
          ];
          if(request('receive_type') == "delivery"){
           
             
               $data['moved_to_pos_at'] = null;
          }

          CardRequest::where('id' , request('id'))->update($data);
     }
     public function updatePayment()
     {
    
          $data = [
               'paid' => request('paid')
          ];


          CardRequest::where('id' , request('id'))->update($data);
     }
}
