<?php

namespace App\Http\Controllers;

use App\Extras\FCMProvider;
use App\Filters\SaleFilters;
use App\Offer;
use App\Sale;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SalesController extends Controller
{

     public function index(SaleFilters $filters)
     {


          return Sale::filter($filters)->with(['user', 'offer', 'merchant'])->latest()->paginate(10);
     }


     public function show($id)
     {

          return Sale::with(['merchant', 'offer', 'user'])->findOrFail($id);
     }

     public function create(FCMProvider $fcm)
     {

          $user = User::find(request('user_id'));


          if (!$user) return response(['message' => 'wrong user id'], 401);

          $offer = Offer::withoutGlobalScopes()->with('merchant')->find(request('offer_id'));

          $merchant = User::find(request('merchant_id'));

          if (!$offer->merchant->is($merchant)) {

               return response(['message' => 'not merchant offer'], 401);
          }

          if ($user->points >= 1) {
               $user->decrement('points',  1);
          }




          $sale = Sale::create([
               'offer_id' => request('offer_id'),
               'user_id' => request('user_id'),
               'merchant_id' => request('merchant_id'),
          ]);

          $sale->load(['offer', 'merchant']);

          $fcm->title('Rate ' . $sale->offer->merchant->fullname)
               ->body($sale->offer->title)
               ->data(['sale_id' => $sale->id, 'offer_id' => $sale->offer->id], 'sale')
               ->send([$user->token]);

          return compact('user', 'sale');
     }

     public function total()
     {

          return Sale::count();
     }

     public function overmonths()
     {
          $sc = Sale::whereYear('created_at', Carbon::now()->year)->get();

          $months = [];


          for ($m = 1; $m <= 12; ++$m) {
               $months[date('F', mktime(0, 0, 0, $m, 1))] = 0;
          }


          $sc = $sc->groupBy(function ($sc) {
               return $sc->created_at->format('F');
          })->map(function ($sc) {
               return $sc->count();
          });

          return collect($months)->merge($sc)->values();
     }

     public function user()
     {
          return Sale::with(['merchant', 'offer'])->where('user_id', auth()->id())->latest()->paginate(10);
     }
}
