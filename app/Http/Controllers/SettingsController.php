<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class SettingsController extends Controller
{
     public function user(Request $request)
     {

          $user = $request->user();

          $user['roles'] = ['admin'];

          return ['data' => $user];
     }

     public function username(Request $request)
     {
          $this->validate($request, [
               'username' => 'required'
          ]);

          $request->user()->update([
               'name' => $request->input('username')
          ]);
     }

     public function password(Request $request)
     {
          $this->validate($request, [
               'password' => 'required'
          ]);

          $request->user()->update([
               'password' => Hash::make($request->input('password'))
          ]);
     }

     public function number(Request $request)
     {

          $this->validate($request, [
               'number' => 'required'
          ]);


          $otp = rand(1000, 9000);

          $endpoint = "https://smsmisr.com/api/webapi/";
          $client = new \GuzzleHttp\Client();


          $msg = "Change Mobile OTP :";

          if ($request->has('hash') && $request->input('hash') != "") {

               $msg = "<#> " . $msg . ' ' . $otp . "\n"  . $request->input('hash');
          } else {
               $msg = "<#> " .  $msg . ' ' . $otp;
          }

          auth()->user()->update(['otp' => $otp]);

          $response = $client->request('POST', $endpoint, ['query' => [
               'username' => 'QC2fV8qL',
               'password' => '6GuMA7unyI',
               'language' => 1,
               'Mobile' => $request->number,
               'sender' => 'Benefits',
               'message' =>  $msg
          ]]);
     }
}
