<?php

namespace App\Http\Controllers;

use App\Extras\FCMProvider;
use App\User;
use Illuminate\Http\Request;

class SosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FCMProvider $fcm)
    {

        $users = User::whereIn('id', request('ids'));

        $users->update(['sos' => true]);



        $fcm->title('')
            ->body('')
            ->data(['can' => true], 'sos')
            ->silent(true)
            ->send($users->get()->pluck('token')->toArray());
    }


    public function sendSosHandshake()
    {



        $endpoint = "https://smsmisr.com/api/webapi/";
        $client = new \GuzzleHttp\Client();

        $number = "";

        if(request()->filled('card_number')){
            $number = request('card_number');
        }else{

            $card = auth()->user()->cards()->whereHas('cardType', function ($q) {
                return $q->where('type', 'normal');
            })->inRandomOrder()->first();

            if($card){
                $number = $card->number;
            }
        }

      

        $msg = "https://maps.google.com?q=" . request('lat')  . "," . request('lng');


        $msg .= "\n" . "User Name : " . auth()->user()->fullname;

        if ($number != "") {

            $msg .=  "\n" . "Card Number : " . $number;
        }

        $client->request('POST', $endpoint, ['query' => [
            'username' => 'QC2fV8qL',
            'password' => '6GuMA7unyI',
            'language' => 1,
            'Mobile' =>  '01123800756,01123800756',
            'sender' => 'Benefits',
            'message' =>  $msg
        ]]);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FCMProvider $fcm)
    {
        $users = User::whereIn('id', request('ids'));


        $users->update(['sos' => false]);



        $fcm->title('')
            ->body('')
            ->data(['can' => false], 'sos' , true)
            ->send($users->get()->pluck('token')->toArray());
    }
}
