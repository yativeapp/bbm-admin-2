<?php

namespace App\Http\Controllers;

use Raju\Streamer\Helpers\VideoStream;

class StreamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function stream($url)
    {
        $stream = new VideoStream($url);
            return response()->stream(function() use ($stream) {
                $stream->start();
            });

    }
}