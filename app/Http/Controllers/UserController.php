<?php

namespace App\Http\Controllers;

use App\Card;
use App\Exports\UsersExport;
use App\Filters\UserFilters;
use App\Imports\imports\UsersImport;
use App\Imports\imports\UsersSheetsImport;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Response;

use Illuminate\Support\Facades\File;

use Dilab\Network\SimpleRequest;
use Dilab\Network\SimpleResponse;
use Dilab\Resumable;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserFilters $filters)
    {

        $results = User::customers()->orWhere('type', 'pos')->latest()->filter($filters);

        if (request()->has('paginate') && request('paginate')) {
            return $results->paginate(request('paginate'));
        }

        return $results->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pos()
    {

        return User::with(['cardRequests' => function ($q) {
            return $q->where('moved_to_pos_at', '!=', null);
        }])->findOrFail(request('user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'type' => request()->filled('pos') && request('pos') ? 'pos' : 'user',
            'fullname' => request('fullname'),
            'email' => request('email'),
            'mobile' => request('mobile'),
            'password' => Hash::make(request('password', 'bbm_123')),
        ]);





        if (request()->filled('pos') && request('pos'))  return $user;


        $card_number = null;

        $date = now();

        if (request()->has('auto_generate_number') && request('auto_generate_number')) {


            $lastNumber = User::customers()->count();



            $l = str_pad(++$lastNumber, 4, "0", STR_PAD_LEFT);


            $card_number =  $date->format('Ymd') . '1000' . $l;

            while (Card::where('number', $card_number)->count()) {

                $l = str_pad(++$lastNumber, 4, "0", STR_PAD_LEFT);

                $card_number =  $date->format('Ymd') . '1000' . $l;
            }
        } else {


            if ($request->has('card_number')) {

                if ($request->has('custom_number') && $request->custom_number) {

                    $card_number = $request->card_number;
                } else {

                    $card_number =  $date->format('Ymd') . '1000' . $request->card_number;
                }

                $exsist = Card::where('number', $card_number)->count();

                if ($exsist) {

                    return response()->json([
                        'This Card Number Already exsist'
                    ], 500);
                }
            }
        }


        if (request()->has('carTypes') && request('carTypes')) {
            $carTypes = collect(request('carTypes'))->map(function ($car) use ($user) {
                $car['user_id'] = $user->id;
                return $car;
            })
                ->keyBy('make_id');
            $user->vehicles()->sync($carTypes);
        }


        $valid_thru = \Carbon\Carbon::now()->addMonths(3);

        if (request()->filled('valid_thru')) {

            $x = \Carbon\Carbon::parse(request('valid_thru'))->addDay();

            if (!$valid_thru->isPast()) {
                $valid_thru = $x;
            }
        }


        $card = Card::create([
            'number' => $card_number,
            'user_id' => $user->id,
            'card_type_id' => request('card_type_id'),
            'valid_thru' => $valid_thru,
            'package_id' =>  request('package_id'),
            'verified' => true
        ]);

        $card->user->increment('points',  $card->package->points);



        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'fullname' => 'required',
            'mobile' => 'required|phone:AUTO,EG'
        ]);

        $user = User::where('mobile', $request->input('mobile'))->orWhere('email', $request->input('email'))->first();



        if ($user && !auth()->user()->is($user)) {
            return response()->json(['errors' => ['wrong' => 'Mobile / Email Already Exists , try login']], 422);
        }


        $data = [
            'type' => 'user',
            'fullname' => request('fullname'),
            'email' => request('email'),
            'mobile' => request('mobile'),
            'mobile2' => request('mobile2'),
            'address' => request('address')
        ];

        if (request()->filled('pos') && request('pos')) {
            $data['password'] = Hash::make(request('password', 'bbm_123'));
            $data['type'] = 'pos';
        } else {
            $data['type'] = 'user';
        }

        auth()->user()->update($data);


        if (request()->filled('pos') && request('pos')) {
        } else {
            $this->sendOtp(request(), auth()->user());
        }
    }
    public function adminUpdate($id, Request $request)
    {

        $this->validate($request, [
            'fullname' => 'required',
        ]);

        $user = User::findOrFail($id);

        $user->update([
            'fullname' => request('fullname'),
            'email' => request('email'),
            'mobile' => request('mobile')
        ]);

        if (request()->has('carTypes') && request('carTypes')) {
            $carTypes = collect(request('carTypes'))->map(function ($car) use ($user) {
                $car['user_id'] = $user->id;
                return $car;
            })
                ->keyBy('make_id');
            $user->vehicles()->sync($carTypes);
        }
    }

    public function unlock()
    {

        $user = User::findOrFail(request('id'));

        $user->update([
            'uuid' => null
        ]);
    }
    public function sendOtp(Request $request, $user = null, $msg = 'Your BBM code is:')
    {





        $otp = rand(1000, 9000);

        $endpoint = "https://smsmisr.com/api/webapi/";
        $client = new \GuzzleHttp\Client();


        if ($request->has('hash') && $request->input('hash') != "") {

            $msg = "<#> " . $msg . ' ' . $otp . "\n"  . $request->input('hash');
        } else {
            $msg = "<#> " .  $msg . ' ' . $otp;
        }
        $response = $client->request('POST', $endpoint, ['query' => [
            'username' => 'QC2fV8qL',
            'password' => '6GuMA7unyI',
            'language' => 1,
            'Mobile' => $user ? $user->mobile : auth()->user()->mobile,
            'sender' => 'Benefits',
            'message' =>  $msg

        ]]);



        $user ? $user->update(['otp' => $otp]) : auth()->user()->update(['otp' => $otp]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
    }

    public function import()
    {


        Excel::import(new UsersSheetsImport(),  request()->file('file'));
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
