<?php

namespace App\Http\Controllers;

use App\VehicleMake;
use App\VehicleModel;
use App\VehicleType;

class VehicleController extends Controller
{


     public function makes()
     {

          return VehicleMake::when(request()->filled('name'), function ($query) {
               $query->where('name', 'like',  "%" . request('name') . "%");
          })->get();
     }
     public function models()
     {
          return VehicleModel::when(request()->filled('name'), function ($query) {
               $query->where('name', 'like',  "%" . request('name') . "%");
          })->where('make_id', request('make_id'))->get();
     }
}
