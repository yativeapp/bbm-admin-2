<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantCompany extends Model
{


    protected $fillable = ['name', 'acting', 'type', 'representee', 'mobile', 'mobil2', 'landline', 'created_at'];

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }
}
