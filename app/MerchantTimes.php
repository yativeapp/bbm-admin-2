<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MerchantTimes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id', 'day', 'from', 'to', 'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];


    public function getToAttribute($val)
    {

        if (is_null($val)) return null;

        return Carbon::parse($val)->format('H:i');
    }
    public function getFromAttribute($val)
    {
        if (is_null($val)) return null;
        return Carbon::parse($val)->format('H:i');
    }

    public function merchant()
    {
        return $this->belongsTo(User::class);
    }
}
