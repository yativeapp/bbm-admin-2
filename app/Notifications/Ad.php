<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class Ad extends Notification
{
     use Queueable;

     public $ad;


     /**
      * Create a new notification instance.
      *
      * @return void
      */
     public function __construct($ad)
     {

          $this->ad = $ad;
     }

     /**
      * Get the notification's delivery channels.
      *
      * @param  mixed  $notifiable
      * @return array
      */
     public function via($notifiable)
     {
          return ['database'];
     }


     /**
      * Get the array representation of the notification.
      *
      * @param  mixed  $notifiable
      * @return array
      */
     public function toDatabase($notifiable)
     {
          return [
               'title' => $this->ad->title && !empty($this->ad->title) ? $this->ad->title : 'BBM',
               'body' => Str::limit($this->ad->content, 15),
               'id' => $this->ad->id,
               'type' => 'ad'
          ];
     }
}
