<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class Offer extends Notification
{
    use Queueable;

    public $offer;

    public $title;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($offer , $title)
    {
        
        $this->offer = $offer;

        $this->title = $title;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'title' => $this->title,
            'body' => $this->offer->description,
            'id' => $this->offer->id,
            'cover' => $this->offer->cover
        ];
    }
}
