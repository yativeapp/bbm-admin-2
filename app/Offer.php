<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;


class Offer extends Model
{
     use Favoriteable;
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'title', 'description', 'merchant_id', 'deliverable', 'cover', 'start_date', 'end_date', 'allTime', 'sos',
          'service_time',
          'user_per_time',
          'bookable'
     ];


     protected $with = ['discounts', 'user', 'merchant', 'branches', 'userBookings'];

     protected $appends = ['valid_for', 'expired', 'is_favorite', 'is_booked', 'booking_redeem'];


     protected $dates = ['start_date', 'end_date'];

     protected static function boot()
     {
          parent::boot();
          if (auth()->check() && auth()->user()->type != 'admin') {


               static::addGlobalScope('range', function (Builder $builder) {


                    return  $builder->where(function ($q) {
                         $q->whereDate('start_date', '<=', now()->toDateString())
                              ->whereDate('end_date', '>=', now()->toDateString());
                    })->orWhere('allTime', true);
               });
          }

          static::addGlobalScope('withOutSos', function (Builder $builder) {


               return  $builder->where('sos', false);
          });
     }


     public function getValidForAttribute()
     {
          if ($this->allTime) return false;

          return $this->end_date->lt(now());
     }
     public function getExpiredAttribute()
     {
          if ($this->allTime) return false;


          return  $this->end_date->lt(now());
     }


     public function getIsFavoriteAttribute()
     {

          return $this->isFavorited();
     }
     public function getIsBookedAttribute()
     {
          if (!auth()->check()) return false;

          return  $this->userBookings->count();
     }

     public function getBookingRedeemAttribute()
     {
          if (!auth()->check()) return false;

          return  $this->userBookings->some(function ($booking) {
               return now()->between($booking->booking_at->subMinutes(30), $booking->booking_at->addMinutes(30));
          });
     }




     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }

     public function categories()
     {

          return $this->belongsToMany(Category::class);
     }

     public function merchant()
     {

          return $this->belongsTo(User::class);
     }
     public function user()
     {

          return $this->belongsTo(User::class);
     }
     public function discounts()
     {

          return $this->hasMany(OfferDiscount::class);
     }

     public function sales()
     {

          return $this->hasMany(Sale::class);
     }

     public function cardTypes()
     {

          return $this->belongsToMany(CardType::class);
     }

     public function branches()
     {

          return $this->belongsToMany(MerchantBranch::class);
     }

     public function bookings()
     {

          return $this->hasMany(OfferBooking::class);
     }

     public function userBookings()
     {

          return $this->bookings()->where('user_id', auth()->id())
               ->whereDate('booking_at', '>=', now());
     }
}
