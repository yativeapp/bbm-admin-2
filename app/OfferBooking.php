<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferBooking extends Model
{

    protected $fillable = [
        'offer_id',
        'user_id',
        'booking_at'
    ];

    protected $dates = [
        'booking_at'
    ];

    protected $appends = ['time'];


    public function getTimeAttribute()
    {

        return $this->booking_at->format('h:i A');
    }

    public function scopeFilter($query, $filters)
    {

        return $filters->apply($query);
    }


    public function offer()
    {

        return $this->belongsTo(Offer::class);
    }
    public function user()
    {

        return $this->belongsTo(User::class);
    }
}
