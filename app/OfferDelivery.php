<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferDelivery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','merchant_id', 'offer_id' , 'address' , 'status' , 'lat' , 'lng'
    ];

    protected $with = ['user' , 'offer'];

    public function offer()
    {
         return $this->belongsTo(Offer::class);
    }
    public function user()
    {
         return $this->belongsTo(User::class);
    }
    public function merchant()
    {
         return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, $filters)
    {

        return $filters->apply($query);
    }

}
