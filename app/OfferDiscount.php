<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferDiscount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'offer_id', 'discount', 'card_type_id', 'package_id', 'discount_type'
    ];

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function cardType()
    {
        return $this->belongsTo(CardType::class);
    }

    public function package()
    {
        return $this->belongsTo(CardtypePackage::class);
    }
}
