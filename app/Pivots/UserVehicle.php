<?php

namespace App\Pivots;

use App\VehicleModel;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserVehicle extends Pivot
{

     protected $fillable = [
          'make_id',
          'make_year',
          'model_id',
          'user_id'
     ];



     public function model()
     {
          return $this->belongsTo(VehicleModel::class);
     }
}
