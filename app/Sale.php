<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sale extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'offer_id', 'user_id', 'merchant_id', 'user_rate', 'merchant_rate'
     ];



     public function scopeFilter($query, $filters)
     {

          return $filters->apply($query);
     }

     public function user()
     {

          return $this->belongsTo(User::class, 'user_id');
     }
     public function merchant()
     {

          return $this->belongsTo(User::class, 'merchant_id');
     }

     public function offer()
     {

          return $this->belongsTo(Offer::class);
     }
}
