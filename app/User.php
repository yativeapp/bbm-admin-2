<?php

namespace App;

use App\Pivots\UserVehicle;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Favoriteability, \Malhal\Geographical\Geographical, Notifiable;

    protected static $kilometers = true;

    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname',
        'make_id',
        'model_id',
        'make_year',
        'address',
        'city',
        'mobile2',
        'password', 'all_cars', 'brand', 'uuid', 'points', 'sos', 'created_at', 'owner_name', 'otp', 'blocked_to', 'landline', 'address', 'merchant_type', 'merchant_acting', 'top', 'serial', 'email', 'type', 'job', 'id_no', 'mobile', 'lat', 'lng', 'avatar', 'token'
    ];

    protected $with = ['cards', 'times'];

    protected $dates = ['blocked_to'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = ['is_blocked'];

    public function getIsBlockedAttribute()
    {

        if ($this->blocked_to === null) {
            return false;
        }

        return $this->blocked_to->gt(now());
    }
    public function getAvatarAttribute($val)
    {


        if (is_null($val)) {

            return "https://ui-avatars.com/api/?bold=true&background=fff&color=010048&name=" . $this->fullname;
        }


        return $val;
    }


    public function make()
    {

        return $this->belongsTo(VehicleMake::class);
    }

    public function model()
    {

        return $this->belongsTo(VehicleModel::class);
    }
    // public function modelYear()
    // {

    //     return $this->belongsTo(VehicleType::class , );
    // }


    public function scopeMerchants($query)
    {
        return $query->where('type', 'merchant');
    }

    public function scopeCustomers($query)
    {
        return $query->where('type', 'user');
    }
    public function scopeAdmins($query)
    {
        return $query->where('type', 'admin');
    }

    public function scopeFilter($query, $filters)
    {

        return $filters->apply($query);
    }


    public function cards()
    {

        return $this->hasMany(Card::class);
    }

    public function bookings()
    {

        return $this->hasMany(OfferBooking::class);
    }

    public function cardTypes()
    {

        return $this->belongsToMany(CardType::class);
    }



    public function offers()
    {
        return $this->hasMany(Offer::class, 'merchant_id');
    }

    public function lastOffer()
    {
        return $this->hasMany(Offer::class, 'merchant_id')->latest()->limit(1);
    }
    public function branches()
    {

        return $this->hasMany(MerchantBranch::class, 'merchant_id');
    }
    public function times()
    {

        return $this->hasMany(MerchantTimes::class, 'merchant_id');
    }



    public function carTypes()
    {

        return $this->belongsToMany(CarType::class);
    }

    public function vehicles()
    {

        return $this->belongsToMany(VehicleMake::class, 'vehicle_user', 'user_id', 'make_id')
            ->using(UserVehicle::class)
            ->withPivot(['make_year', 'model_id']);
    }

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function company()
    {

        return $this->hasOne(MerchantCompany::class, 'merchant_id');
    }

    public function sales()
    {

        return $this->hasManyThrough(Sale::class, Offer::class, 'merchant_id');
    }

    public function categories()
    {

        return $this->belongsToMany(Category::class)->distinct();
    }

    public function cardRequests()
    {
        return $this->hasMany(CardRequest::class);
    }
}
