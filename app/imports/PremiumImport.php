<?php

namespace App\Imports;

use App\Imports\premium\Sheet1;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PremiumImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            3 => new Sheet1()
        ];
    }
}
