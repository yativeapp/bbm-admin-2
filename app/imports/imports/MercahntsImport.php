<?php

namespace App\Imports\imports;

use App\CardType;
use App\Category;
use App\Offer;
use App\User;
use Illuminate\Contracts\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class MercahntsImport implements ToModel, WithStartRow, WithCalculatedFormulas
{
    use SkipsErrors;

    private $current_row = 0;

    private $current_merchant;

    private $packages = [];

    private $cardType;

    public function __construct()
    {
        $this->cardType = CardType::findOrFail(request('card_type'));
    }

    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {


        if ($this->current_row === 0) {
            $this->current_row++;

            return $this->handlePackages($row);
        }


        if (is_null($row[1])) {
            return;
        }

        try {

            $date = \Carbon\Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]));
        } catch (\Exception $e) {
            return;
        }

        $serial = (string) str_replace(" ", "", $row[2]);



        $merchant = User::firstOrCreate(['serial' => $serial]);


        $merchant->update([
            'type' => 'merchant',
            'fullname' => $row[1],
            'id_no' => $row[22],
            'landline' => $row[23],
            'mobile' => $row[24],
            'mobile2' => $row[25],
            'owner_name' => $row[15],
            'serial' => $serial,
            'job' => $row[16],
            'created_at' => $date
        ]);


        $this->current_merchant = $merchant->fresh();

        if($merchant->wasRecentlyCreated){

            $merchant->addresses()->create([
                'building_no' => $row[17],
                'street' => $row[18],
                'district1' => $row[19],
                'city' => $row[20],
                'gov' => $row[21],
            ]);


            $merchant->company()->create([
                'name' => $row[1],
                'type' => $row[5],
                'acting' => $row[6],
                'representee' => $row[4],
                'mobile' => $row[13],
                'mobile2' => $row[14],
                'landline' => $row[12],
                'created_at' => $date
            ]);


            $branch = $merchant->branches()->create([
                'name' => 'المقر الرئيسى',
                'mobile' => $row[13],
                'address_title' => $row[7] . ' ' . $row[8] . ' ' . $row[9] . ' ' . $row[10] . ' ' . $row[11]
            ]);



            $branch->addresses()->create([
                'building_no' => $row[7],
                'street' => $row[17],
                'district1' => $row[9],
                'district2' => $row[10],
                'city' => $row[9],
                'gov' => $row[11]
            ]);

            $this->handleOffers($row, $merchant);

        }else{

            $merchant->addresses()->first()->update([
                'building_no' => $row[17],
                'street' => $row[18],
                'district1' => $row[19],
                'city' => $row[20],
                'gov' => $row[21],
            ]);



            $merchant->company->update([
                'name' => $row[1],
                'type' => $row[5],
                'acting' => $row[6],
                'representee' => $row[4],
                'mobile' => $row[13],
                'mobile2' => $row[14],
                'landline' => $row[12],
                'created_at' => $date
            ]);

        }

      


        if (request()->has('card_type')) {
            $merchant->cardTypes()->syncWithoutDetaching([request('card_type')]);
        }

        return $merchant;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 4;
    }

    public function handlePackages($row)
    {


        $packageNames = array_filter([
            $row[43], $row[44], $row[45], $row[46], $row[47], $row[48], $row[49]
        ]);




        $this->packages = collect($packageNames)->filter()->map(function ($p) {

            if ($p != 'Cancelled') {
                return  $this->cardType->packages()->firstOrCreate([
                    'name' => $p,
                    'points' => 10
                ]);
            }
        });
    }

    public function handleOffers($row, $merchant)
    {


        $offers = [];

        $columns = [
            'category', 'discount', 'short_description', 'long_description', 'address'
        ];

        $packageNames = $this->packages->pluck('name')->toArray();


        $columns = array_merge($columns, $packageNames);



        for ($i = 26; $i < sizeof($row); $i = $i + 12) {


            $data = [];

            collect($columns)->each(function ($col, $t) use ($columns, $i, $row, &$data) {



                $data[$col] = $row[$i + $t];
            });


            $offers[] = $data;
        }


        collect($offers)->each(function ($of) use ($merchant) {



            if ($of['short_description'] && $of['long_description']) {


                $offer = Offer::create([
                    'title' => $of['short_description'],
                    'description' => $of['long_description'],
                    'merchant_id' => $this->current_merchant->id,
                    'cover' => request('cover')
                ]);


                if (!is_null($of['category'])) {
                    $category = Category::firstOrCreate([
                        'name' => $of['category'],
                        'icon' => ''
                    ]);

                    $category->cardTypes()->attach($this->cardType->id);

                    $offer->categories()->attach($category->id);

                    $offer->cardTypes()->attach($this->cardType->id);

                    $merchant->cardTypes()->attach($this->cardType->id);

                    $merchant->categories()->attach($category->id);
                }



                $this->packages->each(function ($p) use ($offer, $of) {

                    if ($of['discount']) {
                        $offer->discounts()->create([
                            'offer_id' => $offer->id,
                            'discount' => intval($of['discount']),
                            'card_type_id' => $this->cardType->id,
                            'package_id' => $p->id
                        ]);
                    }
                });
            }
        });
    }
}
