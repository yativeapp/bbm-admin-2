<?php

namespace App\Imports\imports;

use App\Card;
use App\CardType;
use App\CardtypePackage;
use App\Offer;
use App\User;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class UsersImport implements ToCollection, WithStartRow, WithHeadingRow, WithCalculatedFormulas
{

    public function collection(Collection $rows)
    {
      
        foreach ($rows as $row) {

            $this->exc($row->toArray());
        }
    }


    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function exc(array $row)
    {

        if ($row['card_holder_name'] == "" || is_null($row['card_holder_name']) || empty($row['card_holder_name'])) return;

        try {

            $date = \Carbon\Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['issued_on']));
        } catch (\Exception $e) {
            return;
        }


        // $number =  $date->format('Ymd') . '1000' . str_pad($row[""], 4, "0", STR_PAD_LEFT);
        $number = (string) str_replace(" ", "", $row['sn']);


        $user = User::where('email' , $row['email'])->when($row['mobile_1'] != null && !empty($row['mobile_1']) , function($q) use ($row){
            return $q->orWhere('mobile' , $row['mobile_1']);
        })->first();

        if(!$user){
            $user = User::create(['email' => $row['email']]);
        }
    
        $user->update([
            'fullname' => $row['card_holder_name'],
            'email' => $row['email'],
            'job' => $row['job'],
            'telephone' => $row['land_line'],
            'mobile' => $row['mobile_1'],
            'mobile2' => $row['mobile_2'],
            'address' => $row['address'],
            'car_no' => $row['car_no'],
            'car_type' => $row['brand'],
            'id_no' => $row['id'],
            'blocked_to' => strtolower($row['status']) === 'canceled' ? \Carbon\Carbon::now()->addMonth(12) : null
        ]);




        if($user->wasRecentlyCreated){


            if($row['member'] === 'Cancelled') return;

            $data = [
                'name' => $row['member'],
                'card_type_id' => intVal(request('card_type'))
            ]; 

            $p = CardtypePackage::where($data)->first();

            if(!$p){

                $data['points'] = 10;
                
                $p = CardtypePackage::create($data);
            }


           
           $card =  Card::create([
                'number' => $number,
                'user_id' => $user->id,
                'card_type_id' => $p->card_type_id,
                'package_id' => $p->id,
                'issued_by' => isset($row['issued_by']) ? $row['issued_by'] : "",
                'valid_thru' =>strtolower($row['status']) === 'canceled' ?  \Carbon\Carbon::now()->subMonths(3) : \Carbon\Carbon::now()->addMonth(12),
                'verified' => true,
                'created_at' => $date
            ]);

            if(strtolower($row['status']) === 'canceled'){

                $card->delete();

               
            }else{
                $user->increment('points' , 10);
            }
        }


    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
