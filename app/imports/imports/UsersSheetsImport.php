<?php 

namespace App\Imports\imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UsersSheetsImport implements WithMultipleSheets 
{
   
    public function sheets(): array
    {
        return [
            0 => new UsersImport()
        ];
    }
}