<?php

namespace App\Imports\premium;

use App\Card;
use App\CardType;
use App\CardtypePackage;
use App\Offer;
use App\User;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class Sheet1 implements ToCollection, WithStartRow
{

     public function startRow(): int
     {

          return 2;
     }
     public function collection(Collection $rows)
     {

          $basic = CardtypePackage::find(37);
          $premium = CardtypePackage::find(35);

          $numbers = collect($rows)->map(function ($user) {

               return str_replace(" ", "", $user[1]);
          });

          Card::with(['package', 'user'])->whereIn('number', $numbers)->get()->each(function ($card) use ($basic, $premium) {


               if ($card->package_id != $premium->id) {
                    $card->update([
                         'package_id' => $premium->id
                    ]);



                    if ($card->user->points > $basic->points) {

                         $card->user->decrement('points',  $basic->points);
                    }

                    $card->user->update(['sos' => true]);

                    $card->user->increment('points',  $premium->points);
               }
          });
     }
}
