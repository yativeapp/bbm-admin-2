<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '734193057116459',
        'client_secret' => '418ead45d3e8c38306d59e06773348cb',
        'redirect' => 'http://35.234.153.238/',
    ],
    "apple" => [
        "client_id" => '1511599712',
        "client_secret" => 'eyJraWQiOiJETUo5NEMzNDk1IiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiI2MzROWlk4NzJVIiwiaWF0IjoxNTkwMTg1MzU3LCJleHAiOjE2MDU3MzczNTcsImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJ1c2VyLmJibWVneS5jbGllbnQifQ.imfy6savAuIpToFY7Z_68HdpFAsMFSd4u7aOQdJTv9EFOtne66JY1BYBfOmzRjrer0dfnFc2_dB6eSm3HiZJng',
        "redirect" => 'https://bbmegy.com'
    ],

];
