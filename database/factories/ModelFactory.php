<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Card;
use App\CardRequest;
use App\CardType;
use App\CardtypePackage;
use App\Cartype;
use App\Category;
use App\MerchantBranch;
use App\User;
use App\Offer;
use App\OfferDiscount;
use Faker\Generator as Faker;
use Plansky\CreditCard\Generator;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'fullname' => $faker->name,
        'password' => 'das',
        'email' => $faker->email,
        'job' => $faker->jobTitle,
        'id_no' => $faker->randomDigitNotNull,
        'mobile' => $faker->phoneNumber,
        'type' => 'user',
        'serial' => '202012990000001'
    ];
});
$factory->define(MerchantBranch::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'mobile' => $faker->phoneNumber,
        'address_title' => $faker->text,
        'lat' => 30.044420,
        'lng' => 31.235712,
        'merchant_id' => factory('App\User')->create(['type' => 'merchant'])->id
    ];
});

$factory->define(Card::class, function (Faker $faker) {

    $date = \Carbon\Carbon::now()->addMonth(3);

    return [
        'number' => '2020030110000001',
        'user_id' => factory('App\User'),
        'card_type_id' => factory('App\CardType'),
        'package_id' => 1,
        'valid_thru' => $date,
        'verified' => true
    ];
});
$factory->define(CardRequest::class, function (Faker $faker) {

    return [
        'user_id' => factory('App\User'),
        'card_type_id' => factory('App\CardType'),
    ];
});
$factory->define(CardType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(10),
        'cover' => '',
        'type' => 'normal'
    ];
});
$factory->define(CardTypePackage::class, function (Faker $faker) {
    return [
        'card_type_id' => factory('App\CardType'),
        'name' => $faker->name,
        'points' => 50
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => 'fas fa-angry'
    ];
});

$factory->define(Offer::class, function (Faker $faker) {
    return [
        'title' => $faker->text(10),
        'description' => $faker->text,
        'sos' => false,
        'merchant_id' => factory('App\User')->create(['type' => 'merchant'])->id,
        'cover' => 'https://www.pngitem.com/pimgs/m/193-1934921_special-offers-png-discount-offer-vector-png-transparent.png'
    ];
});
$factory->define(OfferDiscount::class, function (Faker $faker) {
    return [
        'offer_id' => factory('App\Offer'),
        'card_type_id' => factory('App\CardType'),
        'package_id' => 1,
        'discount' => 50
    ];
});
$factory->define(Cartype::class, function (Faker $faker) {
    return [
        'name' => $faker->text(5)
    ];
});
