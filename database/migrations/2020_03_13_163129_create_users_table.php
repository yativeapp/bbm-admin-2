<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable()->index();
            $table->string('password')->nullable();
            $table->string('otp')->default('');
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->text('fullname')->nullable();
            $table->text('owner_name')->nullable();
            $table->string('id_no')->nullable();
            $table->string('job')->nullable();
            $table->longText('token')->nullable();
            $table->longText('serial')->nullable();
            $table->longText('address')->nullable();
            $table->string('car_no')->nullable();
            $table->string('mobile')->nullable();
            $table->string('mobile2')->nullable();
            $table->string('landline')->nullable();
            $table->longText('uuid')->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('top')->default(false);
            $table->boolean('brand')->default(false);
            $table->boolean('sos')->default(false);
            $table->unsignedBigInteger('points')->default(0);
            $table->unsignedBigInteger('service_time')->default(0);
            $table->unsignedBigInteger('user_per_time')->default(0);
            $table->enum('type', ['admin', 'user', 'merchant', 'pos'])->default('user');
            $table->string('api_token', 60)->unique()->nullable();
            $table->timestamp('blocked_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
