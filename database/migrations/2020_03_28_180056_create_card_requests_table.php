<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('card_type_id');
            $table->unsignedInteger('package_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('car_type')->nullable();
            $table->string('payment_method');
            $table->enum('receive_type' , ['delivery' , 'pos']);
            $table->enum('paid', ['paid', 'not_paid'])->default('not_paid');
            $table->longText('address');
            $table->timestamp('valid_thru')->nullable();
            $table->timestamp('moved_to_pos_at')->nullable();
            $table->timestamps();

            $table->softDeletes();

            // $table->addColumn('integer' , 'package_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_requests');
    }
}
