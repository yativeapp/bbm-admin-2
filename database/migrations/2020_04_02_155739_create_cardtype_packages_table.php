<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardtypePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardtype_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('card_type_id');
            $table->unsignedBigInteger('points');
            $table->unsignedBigInteger('price')->default(0);
            $table->string('name');
            $table->boolean('in_stock')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardtype_packages');
    }
}
