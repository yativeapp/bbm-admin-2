<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantBranchOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_branch_offer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('merchant_branch_id');
            $table->unsignedBigInteger('offer_id');
            $table->timestamps();


            $table->foreign('merchant_branch_id')
                ->references('id')->on('merchant_branches')
                ->onDelete('cascade');
            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_branch_offer');
    }
}
