<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesConstrains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return;
        // Schema::disableForeignKeyConstraints();

        Schema::table('car_type_user', function (Blueprint $table) {

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            // $table->foreign('car_type_id')
            //     ->references('id')->on('car_types')
            //     ->onDelete('cascade');
        });


        Schema::table('cards', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('package_id')
                ->references('id')->on('cardtype_packages')
                ->onDelete('cascade');
            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('sales', function (Blueprint $table) {

            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('merchant_id')->change();
            $table->unsignedBigInteger('offer_id')->change();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('merchant_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');
        });

        Schema::table('card_requests', function (Blueprint $table) {

            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('package_id')->change();
            $table->unsignedBigInteger('card_type_id')->change();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('package_id')
                ->references('id')->on('cardtype_packages')
                ->onDelete('cascade');
            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('category_user', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('category_id')->change();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });

        Schema::table('card_type_user', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('card_type_id')->change();


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('offer_openings', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('offer_id')->change();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');
        });



        Schema::table('card_type_category', function (Blueprint $table) {

            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });

        Schema::table('category_offer', function (Blueprint $table) {


            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });

        Schema::table('ad_times', function (Blueprint $table) {


            $table->foreign('ad_id')
                ->references('id')->on('ads')
                ->onDelete('cascade');
        });

        Schema::table('merchant_companies', function (Blueprint $table) {


            $table->foreign('merchant_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::table('cardtype_packages', function (Blueprint $table) {
            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('offer_discounts', function (Blueprint $table) {



            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');

            $table->foreign('package_id')
                ->references('id')->on('cardtype_packages')
                ->onDelete('cascade');

            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('merchant_branches', function (Blueprint $table) {

            $table->foreign('merchant_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::table('card_type_offer', function (Blueprint $table) {



            $table->foreign('offer_id')
                ->references('id')->on('offers')
                ->onDelete('cascade');

            $table->foreign('card_type_id')
                ->references('id')->on('card_types')
                ->onDelete('cascade');
        });

        Schema::table('offers', function (Blueprint $table) {


            $table->foreign('merchant_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
