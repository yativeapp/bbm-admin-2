<?php

use App\VehicleMake;
use App\VehicleModel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\User::truncate();
        // $this->call('CategorySeeder');
        // $this->call('MerchantsSeeder');
        // $this->call('UserTableSeeder');
        // $this->call('OfferSeeder');
        // $this->call('CarTypes');
        // $this->call('VehicleMakesSeeder');
        // $this->call('VehicleModelsSeeder');
        // $this->call('VehicleTypesSeeder');

        $cherry  = VehicleMake::create([
            'name' => 'Cherry',
            'code' => 'Cherry'
        ]);

        VehicleModel::insert([
            ['make_id' => $cherry->id, 'name' => 'Tiggo4', 'code' => 'Tiggo4'],
            ['make_id' => $cherry->id, 'name' => 'Tiggo7', 'code' => 'Tiggo7'],
            ['make_id' => $cherry->id, 'name' => 'Tiggo8', 'code' => 'Tiggo8'],
            ['make_id' => $cherry->id, 'name' => 'Arrizo', 'code' => 'Arrizo'],
            ['make_id' => $cherry->id, 'name' => 'Tiggo3', 'code' => 'Tiggo3'],
            ['make_id' => $cherry->id, 'name' => 'Arrizo 5', 'code' => 'Arrizo 5'],
        ]);
    }
}
