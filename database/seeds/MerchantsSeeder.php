<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MerchantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $merchants = [
            [
                'email' => 'dsada@gmail.com',
                'fullname' => 'Fingraft Gift Shop',
                'password' => Hash::make('w'),
                'type' => 'merchant',
                'job' => '',
                'mobile' => '',
            ],
            [
                'email' => 'dsada@gmail.com',
                'fullname' => 'Pepenero',
                'password' => Hash::make('w'),
                'type' => 'merchant',
                'job' => '',
                'mobile' => '',
            ]
        ];

        // collect($merchants)->each(function ($m) {
        //     \App\User::create($m);
        // });
    }
}
