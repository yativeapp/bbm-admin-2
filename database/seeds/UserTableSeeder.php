<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        factory('App\User')->create([
            'fullname' => 'BBM ADMIN',
            'email' => 'admin',
            'password' => Hash::make('password'),
            'type' => 'admin',
        ]);
        factory('App\User')->create([
            'fullname' => 'wkasem',
            'email' => 'wkasem22@gmail.com',
            'password' => Hash::make('12345'),
            'type' => 'user',
        ]);

        factory('App\User')->create([
            'fullname' => 'apple',
            'email' => 'test@apple.com',
            'password' => Hash::make('appletest'),
            'type' => 'user',
        ]);

        factory('App\User')->create([
            'fullname' => 'facebook',
            'email' => 'test@fb.com',
            'password' => Hash::make('testfb'),
            'type' => 'user',
        ]);


        // factory('App\CardRequest', 10)->create();
    }
}
