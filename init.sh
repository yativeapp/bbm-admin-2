php artisan migrate:fresh
php artisan db:seed
php artisan passport:install --force
chmod 755 ./storage/oauth-public.key
chmod 755 ./storage/oauth-private.key