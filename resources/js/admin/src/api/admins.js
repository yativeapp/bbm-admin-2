import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/admins`,
          method: 'get'
     }, params);

}
export function create(data) {

     return request({
          url: `/admins`,
          method: 'post',
          data
     })
}
export function remove(id) {

     return request({
          url: `/admins/${id}`,
          method: 'delete'
     })
}



export function login(data) {

     return request({
          url: '/merchant/login',
          method: 'post',
          data
     })
}
export function block(user) {

     return request({
          url: 'user/block',
          method: 'post',
          data: { id: user.id }
     })
}
export function unlock(user) {

     return request({
          url: 'user/unlock',
          method: 'post',
          data: { id: user.id }
     })
}

export function getInfo(token) {
     return request({
          url: '/user/info',
          method: 'get',
          params: { token }
     })
}

export function logout() {
     return request({
          url: '/user/logout',
          method: 'post'
     })
}
