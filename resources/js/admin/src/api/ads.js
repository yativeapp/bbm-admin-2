import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/ads`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/ads`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `ads/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `ads/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/ads/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/ads/overmonths`,
          method: 'get',
     })
}