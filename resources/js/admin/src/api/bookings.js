import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/bookings`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/bookings`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `bookings/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `bookings/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/bookings/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/bookings/overmonths`,
          method: 'get',
     })
}