import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/merchant-branches`,
          method: 'get'
        }, params);


}
export function create(data) {

     return request({
          url: `/merchant-branches`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `merchant-branches/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `merchant-branches/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/merchant-branches/total`,
          method: 'get',
     })
}


export function overmonths() {

     return request({
          url: `/merchant-branches/overmonths`,
          method: 'get',
     })
}