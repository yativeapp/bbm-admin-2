import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/car-types`,
          method: 'get'
     }, params);
}


export function makes(params) {

     return new filterRequest({
          url: `/vehicle/makes`,
          method: 'get'
     }, params);
}

export function models(params) {

     return new filterRequest({
          url: `/vehicle/models`,
          method: 'get'
     }, params);
}

export function create(data) {

     return request({
          url: `/car-types`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `car-types/${id}`,
          method: 'patch',
          data
     })
}
