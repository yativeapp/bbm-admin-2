import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/card-requests`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/cards`,
          method: 'post',
          data
     })
}


export function remove(id) {

     return request({
          url: `/card-requests/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/card-requests/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/card-requests/overmonths`,
          method: 'get',
     })
}

export function updateDelivery(data) {

     return request({
          url: '/card-requests/update-delivery',
          method: 'patch',
          data
     })
}
export function updatePayment(data) {

     return request({
          url: '/card-requests/update-payment',
          method: 'patch',
          data
     })
}
