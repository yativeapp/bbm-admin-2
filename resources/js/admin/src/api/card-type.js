import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {
     return new filterRequest({
          url: `/card-types`,
          method: 'get'
     }, params);

}
export function create(data) {

     return request({
          url: `/new-card-type`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `card-type/${id}/update`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `/card-type/${id}/destroy`,
          method: 'delete',
     })
}