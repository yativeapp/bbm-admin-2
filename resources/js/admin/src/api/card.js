import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/cards`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/cards`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `cards/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `/cards/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/cards/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/cards/overmonths`,
          method: 'get',
     })
}