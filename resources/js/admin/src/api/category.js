import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/categories`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/new-category`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `category/${id}/update`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `/category/${id}/destroy`,
          method: 'delete',
     })
}