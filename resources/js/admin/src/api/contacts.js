import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/contact`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/contact`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `contact/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `/contact/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/contact/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/contact/overmonths`,
          method: 'get',
     })
}