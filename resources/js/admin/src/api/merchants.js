import request from '@/utils/request'

import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/merchants`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/new-merchant`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `merchants/${id}/update`,
          method: 'patch',
          data
     })
}
export function toggleTop(data) {

     return request({
          url: `merchants/toggleTop`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `merchants/${id}/destroy`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/merchants/total`,
          method: 'get',
     })
}


export function overmonths() {

     return request({
          url: `/merchants/overmonths`,
          method: 'get',
     })
}