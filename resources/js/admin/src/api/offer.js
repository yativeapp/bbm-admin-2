import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/offers`,
          method: 'get'
     }, params);
}
export function create(data) {

     return request({
          url: `/offers`,
          method: 'post',
          data
     })
}
export function notify(id) {

     return request({
          url: `/noti/offer/${id}`,
          method: 'post'
     })
}
export function update(id, data) {

     return request({
          url: `offers/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `offers/${id}`,
          method: 'delete',
     })
}

export function total() {

     return request({
          url: `/offers/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/offers/overmonths`,
          method: 'get',
     })
}