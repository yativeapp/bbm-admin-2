import request from '@/utils/request'

import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/delivery`,
          method: 'get'
     }, params);
}

export function update(data) {

     return request({
          url: 'delivery/order/change-status',
          method: 'patch',
          data
     })
}

