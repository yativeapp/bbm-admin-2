import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/packages`,
          method: 'get'
     }, params);

}
export function create(data) {

     return request({
          url: `/packages`,
          method: 'post',
          data
     })
}
export function update(id, data) {

     return request({
          url: `packages/${id}`,
          method: 'patch',
          data
     })
}

export function remove(id) {

     return request({
          url: `/packages/${id}`,
          method: 'delete',
     })
}