import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

     return new filterRequest({
          url: `/sales`,
          method: 'get'
     }, params);


}

export function total() {

     return request({
          url: `/sales/total`,
          method: 'get',
     })
}

export function overmonths() {

     return request({
          url: `/sales/overmonths`,
          method: 'get',
     })
}