import request from '@/utils/request'
import filterRequest from '@/utils/filterRequest';

export function get(params) {

  return new filterRequest({
    url: `/users`,
    method: 'get'
  }, params);

}
export function create(data) {

  return request({
    url: `/users`,
    method: 'post',
    data
  })
}
export function update(id, data) {

  return request({
    url: `/user/admin/update/${id}`,
    method: 'patch',
    data
  })
}
export function addSos(ids) {

  return request({
    url: `/users/sos/add`,
    method: 'post',
    data: { ids }
  })
}
export function removeSos(ids) {

  return request({
    url: `/users/sos/remove`,
    method: 'post',
    data: { ids }
  })
}
export function remove(id) {

  return request({
    url: `/users/${id}`,
    method: 'delete'
  })
}



export function login(data) {

  return request({
    url: '/merchant/login',
    method: 'post',
    data
  })
}
export function block(user) {

  return request({
    url: 'user/block',
    method: 'post',
    data: { id: user.id }
  })
}
export function unlock(user) {

  return request({
    url: 'user/unlock',
    method: 'post',
    data: { id: user.id }
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
