import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/dashboard',
    component: Layout,
    children: [{
      path: '',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },

  {
    path: '/categories',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Categories',
        component: () => import('@/views/categories/index'),
        meta: { title: 'Categories', icon: 'form' }
      }
    ]
  },
  {
    path: '/cards',
    name: 'cards',
    meta: { title: 'Cards', icon: 'form' },
    component: Layout,
    children: [
      {
        path: '/memberships',
        name: 'memberships',
        component: () => import('@/views/card-types/index'),
        meta: { title: 'Memberships', icon: 'form' }
      },
      {
        path: '/packages',
        name: 'packages',
        component: () => import('@/views/packages/index'),
        meta: { title: 'Packages', icon: 'form' }
      },
      {
        path: '/issued-cards',
        name: 'issued-cards',
        component: () => import('@/views/cards/index'),
        meta: { title: 'Issued', icon: 'form' }
      },
      {
        path: '/requests',
        name: 'card-requests',
        component: () => import('@/views/requests/index'),
        meta: { title: 'Requests', icon: 'form' }
      }
    ]
  },
  {
    path: '/offers',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Offers',
        component: () => import('@/views/offers/index'),
        meta: { title: 'Offers', icon: 'form' }
      }
    ]
  },
  {
    path: '/bookings',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Bookings',
        component: () => import('@/views/bookings/index'),
        meta: { title: 'Bookings', icon: 'form' }
      }
    ]
  },
  {
    path: '/sales',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Sales',
        component: () => import('@/views/sales/index'),
        meta: { title: 'Sales', icon: 'form' }
      }
    ]
  },
  {
    path: '/users',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Users',
        component: () => import('@/views/users/index'),
        meta: { title: 'Users', icon: 'form' }
      }
    ]
  },
  {
    path: '/pos',
    name: 'pos',
    meta: { title: 'POS', icon: 'form' },
    component: Layout,
    children: [
      {
        path: 'users',
        name: 'POS User',
        component: () => import('@/views/pos/index'),
        meta: { title: 'Users', icon: 'form' }
      },
      {
        path: 'sales',
        name: 'POS Sales',
        component: () => import('@/views/pos/sales/index'),
        meta: { title: 'Sales', icon: 'form' }
      }
    ]
  },
  {
    path: '/suppliers',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Suppliers',
        component: () => import('@/views/merchants/index'),
        meta: { title: 'Suppliers', icon: 'form' }
      },
      {
        path: ':id/branches',
        name: 'SupplierBranches',
        component: () => import('@/views/merchants/branches/index'),
        meta: { title: 'Branches', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/orders',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Orders',
        component: () => import('@/views/orders/index'),
        meta: { title: 'Orders', icon: 'form' }
      }
    ]
  },
  {
    path: '/admins',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Admins',
        component: () => import('@/views/admins/index'),
        meta: { title: 'Admins', icon: 'form' }
      }
    ]
  },

  {
    path: '/contacts',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Contact',
        component: () => import('@/views/contacts/index'),
        meta: { title: 'Contact', icon: 'form' }
      }
    ]
  },
  {
    path: '/ads',
    component: Layout,
    children: [
      {
        path: '',
        name: 'ADS',
        component: () => import('@/views/ads/index'),
        meta: { title: 'ADS', icon: 'form' }
      }
    ]
  },
  {
    path: '/car-types',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Car types',
        component: () => import('@/views/car-types/index'),
        meta: { title: 'Car types', icon: 'form' }
      }
    ]
  },



  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/dashboard', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
