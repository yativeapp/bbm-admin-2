
import request from '@/utils/request'

export default class filterRequest {


     filters = {};


     requestObj = null;

     constructor(requestObj, filters = {}) {

          this.requestObj = requestObj;

          this.setFilters(filters);

          return this.sendRequest();
     }

     setFilter(name, value) {

          this.filters[name] = value;
     }

     setFilters(Obj) {

          Object.keys(Obj).forEach(key => this.setFilter(key, Obj[key]));
     }


     sendRequest() {

          return request({
               ...this.requestObj,
               params: { ...this.filters }
          })
     }


}