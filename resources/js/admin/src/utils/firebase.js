import Vue from 'vue';
var firebase = require('firebase/app');
require('firebase/storage');

firebase.initializeApp({
     apiKey: "AIzaSyA7Ie_VJzlDWzriXRnHmFV0vfH8PuL0O-c",
     authDomain: "bbm-back.firebaseapp.com",
     databaseURL: "https://bbm-back.firebaseio.com",
     projectId: "bbm-back",
     storageBucket: "bbm-back.appspot.com",
     messagingSenderId: "949780889469",
     appId: "1:949780889469:web:76054c2b4ff253947e9614"
});

Vue.prototype.$firebase = {

     storage() {
          return firebase.storage().ref();
     }
}
