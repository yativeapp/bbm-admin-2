import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
     load: {
          key: 'AIzaSyDS8R4ZihTWS1HH90eAsv9uAFqQ3DHciXI',
          libraries: 'places',

     },
     installComponents: true
})