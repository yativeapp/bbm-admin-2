<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'خطأ فى الأيميل أو كلمة السر',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    "unvalidUuid" => "هذا جهاز جديد , برجاء الأتصال بخدمة العملاء لتفعيل هذا الجهاز",
    "no_card" => "لا يوجد بطاقة بهذا الرقم",
    "no_serial" => "لا يوجد بائع بهذا الرقم"

];
