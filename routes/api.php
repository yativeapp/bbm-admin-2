<?php

use App\Card;
use App\CardtypePackage;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/auth-user', 'AuthController@user');

Route::get('user/coupons', ['as' => 'card-type.index', 'uses' =>  'CardTypeController@userCoupons']);
Route::get('user/coupons2', ['as' => 'card-type.index2', 'uses' =>  'CardTypeController@userCoupons2']);


//offers
Route::get('offers/total', ['as' => 'offers.total', 'uses' =>  'OfferController@total']);
Route::get('offers/overmonths', ['as' => 'offers.overmonths', 'uses' =>  'OfferController@overmonths']);

Route::get('cards/total', ['as' => 'cards.total', 'uses' =>  'CardController@total']);
Route::get('cards/overmonths', ['as' => 'cards.overmonths', 'uses' =>  'CardController@overmonths']);


Route::patch('user/update', 'UserController@update');


Route::post('send-otp', 'AuthController@sendOtp');
Route::post('verify/otp', 'AuthController@otpVerify');

Route::post('user/new-card', ['as' => 'user.new-card', 'uses' =>  'CardController@store']);

Route::get('user/notis', ['uses' =>  'AuthController@notis']);

Route::patch('/sale/user/rate', 'RateController@userRate');
Route::patch('/sale/merchant/rate', 'RateController@merchantRate');


//users
Route::resource('users', 'UserController');
Route::patch('user/admin/update/{id}', 'UserController@adminUpdate');
Route::get('packages/card-types', 'PackagesController@cardTypes');
Route::resource('packages', 'PackagesController');

Route::patch('user/avatar/update', 'AuthController@updateAvatar');

Route::get('user/transactions', 'SalesController@user');

Route::post('/admin/noti', 'NotificationsController@admin');
Route::post('/sos/offers', 'NotificationsController@sendSosOffers');

Route::get('user/ad', 'AdsController@get');

Route::resource('ads', 'AdsController')->except([
    'show'
]);

Route::get('/vehicle/makes', 'VehicleController@makes');
Route::get('/vehicle/models', 'VehicleController@models');

//contacts
Route::resource('contact', 'ContactController');

Route::get('favourite', 'FavouriteController@index');
Route::post('favourite', 'FavouriteController@create');


//seetings
Route::patch('/user/change/username', 'SettingsController@username');
Route::patch('/user/change/password', 'SettingsController@password');
Route::patch('/user/change/number', 'SettingsController@number');



Route::post('noti/offer/{id}', 'NotificationsController@offer');


Route::get('merchant/branches', 'MerchantController@branches');
Route::get('merchant/bookings', 'MerchantController@bookings');

Route::resource('merchant-branches', 'MerchantBranchesController');

Route::get('branches/autocomplete', 'MerchantBranchesController@autocomplete');

//delivery
Route::resource('delivery', 'DeliveryController');
Route::patch('delivery/order/change-status', 'DeliveryController@changeStatus');

//car Types
Route::resource('car-types', 'CarTypeController');

//brands
Route::get('brands', 'BrandsController@index');

//card-requests
Route::patch('/card-requests/update-delivery', 'RequestsController@updateDelivery');
Route::patch('/card-requests/update-payment', 'RequestsController@updatePayment');
Route::resource('card-requests', 'RequestsController');


Route::post('pos/user/details', 'UserController@pos');

Route::resource('pos', 'PosController');
//cards
Route::resource('cards', 'CardController');

Route::post('user/card/verifyCard', 'CardTypeController@verifyCard');

Route::get('user/memberships', 'AuthController@memberships');

Route::get('card-types', ['as' => 'card-type.index', 'uses' =>  'CardTypeController@index']);
Route::resource('offers', 'OfferController');
Route::resource('bookings', 'BookingController');
//card-types
Route::get('card-gifts', ['as' => 'card-type.index', 'uses' =>  'CardTypeController@gifts']);
Route::post('card-gifts/claim', ['as' => 'card-type.index', 'uses' =>  'CardTypeController@claimGift']);
Route::get('card-type/{id}', ['as' => 'card-type.show', 'uses' =>  'CardTypeController@show']);
Route::post('new-card-type', ['as' => 'card-type.store', 'uses' =>  'CardTypeController@store']);
Route::patch('card-type/{id}/update', ['as' => 'card-type.patch', 'uses' =>  'CardTypeController@patch']);
Route::delete('card-type/{id}/destroy', ['as' => 'card-type.destroy', 'uses' =>  'CardTypeController@destroy']);
Route::get('user/gifts', ['as' => 'card-type.index', 'uses' =>  'CardTypeController@userGifts']);

//sales
Route::get('sales', ['as' => 'sales.index', 'uses' =>  'SalesController@index']);

Route::post('sales', ['as' => 'sales.index', 'uses' =>  'SalesController@create']);
Route::get('sales/total', ['as' => 'sales.total', 'uses' =>  'SalesController@total']);
Route::get('sales/overmonths', ['as' => 'sales.overmonths', 'uses' =>  'SalesController@overmonths']);
Route::get('sales/{id}', ['as' => 'sales.index', 'uses' =>  'SalesController@show']);

//merchants
Route::get('merchants', ['as' => 'merchants.index', 'uses' =>  'MerchantController@index']);
Route::get('merchants2', ['as' => 'merchants.index2', 'uses' =>  'MerchantController@index2']);
Route::get('merchants/total', ['as' => 'merchants.total', 'uses' =>  'MerchantController@total']);
Route::get('merchants/overmonths', ['as' => 'merchants.overmonths', 'uses' =>  'MerchantController@overmonths']);
Route::get('merchants/{id}', ['as' => 'merchants.show', 'uses' =>  'MerchantController@show']);
Route::post('new-merchant', ['as' => 'merchants.store', 'uses' =>  'MerchantController@store']);
Route::patch('merchants/{id}/update', ['as' => 'merchants.patch', 'uses' =>  'MerchantController@patch']);
Route::delete('merchants/{id}/destroy', ['as' => 'merchants.destroy', 'uses' =>  'MerchantController@destroy']);


//geo
Route::get('nearby', ['as' => 'geo.get', 'uses' =>  'GeoController@get']);


//category
Route::get('categories', ['as' => 'category.index', 'uses' =>  'CategoryController@index']);
Route::get('category/{id}', ['as' => 'category.show', 'uses' =>  'CategoryController@show']);
Route::get('category/{id}/merchants', ['as' => 'category.merchants', 'uses' =>  'CategoryController@merchants']);
Route::post('new-category', ['as' => 'category.store', 'uses' =>  'CategoryController@store']);
Route::patch('category/{id}/update', ['as' => 'category.patch', 'uses' =>  'CategoryController@patch']);
Route::delete('category/{id}/destroy', ['as' => 'category.destroy', 'uses' =>  'CategoryController@destroy']);


Route::post('canRedeem', ['as' => 'geo.canRedeem', 'uses' =>  'GeoController@canRedeem']);
Route::get('user/info', 'AuthController@user');


Route::patch('/fcm/token', 'AuthController@updateToken');





Route::patch('merchants/toggleTop', 'MerchantController@toggleTop');


Route::resource('user/block', 'BlockController');
Route::post('user/unlock', 'UserController@unlock');

Route::post('sms/resend', 'AuthController@resendCode');


Route::get('showSos', 'AuthController@showSos');
Route::post('sos/handshake', 'SosController@sendSosHandshake');


Route::resource('admins', 'AdminController');


Route::post('users/sos/add', 'SosController@store');
Route::post('users/sos/remove', 'SosController@destroy');


Route::post('payment', 'RequestsController@pay');
Route::post('infoFillup', 'AuthController@infoFillup');

Route::get('user/bookings', 'BookingController@userBookings');
