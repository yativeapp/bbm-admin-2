<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Card;
use App\CardtypePackage;
use App\Extras\Accept;
use App\Extras\FCMProvider;
use App\Imports\PremiumImport;
use App\Sale;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Facades\Excel;

Route::get('/', function () {



     // $basic = CardtypePackage::find(37);

     // Card::with(['package', 'user'])->get()->each(function ($card) use ($basic) {

     //      $card->update([
     //           'prev_package_id' => $card->package_id,
     //           'package_id' => $basic->id
     //      ]);

     //      $card->user->increment('points',  $basic->points);
     // });



     // return User::whereHas('cards', function ($query) {
     //      return $query->where('number', '2020030110000201');
     // })->first();


     // $fcm = new FCMProvider;


     // $token = "ek3bMG3sD9M:APA91bGHS3Oysoj5eJKlLOin3EmW5Rq3fMXggzp9lhWUFl83e3NTNAwxb7Xsz73LQ7DYFyCA79M297_z1MMR82Dtou1MOUDMTpdfZprDFfdHS227pDY27_GHzpQK2tsAv-5Sr-NgG0pK";

     // $fcm->data([])
     // ->send([$token]);

     return view('index');
});
Route::get('/privacy', function () {


     return view('privacy');
});
Route::get('/xs', function () {




     User::get()->each(function ($user) {

          $vehicle = [$user->make_id => ["make_id" => $user->make_id, "model_id" => $user->model_id, "make_year" => $user->make_year]];

          $user->vehicles()->attach($vehicle);
     });

     // Excel::import(new PremiumImport(),  storage_path('uploads/App.xlsx'));
});
Route::get('/init', function () {

     // print_r(phpinfo());
     // shell_exec('composer install');

     Artisan::call('key:generate');
     Artisan::call('migrate:refresh --seed');
     // Artisan::call('passport:install');
});

Route::post('user/login', 'AuthController@login')->name('login');
Route::post('user/logout', 'AuthController@logout');
Route::post('user/card/login', 'AuthController@cardLogin');
Route::post('user/serial/login', 'AuthController@serialLogin');
Route::post('user/signup', 'AuthController@signup')->name('signup');
Route::post('user/fb/login', 'AuthController@fbLogin');
Route::post('user/forget', 'AuthController@forget');
Route::post('user/otp/verify', 'AuthController@changeOtpPassword');
Route::post('merchant/login', 'AuthController@mer');

Route::get('ad/test', 'AdsController@test');



Route::post('import/merchants', 'MerchantController@import');
Route::post('import/users', 'UserController@import');

Route::post('export/merchants', 'MerchantController@export');
Route::post('export/users', 'UserController@export');


Route::get('ads/{id}', 'AdsController@show');;

Route::get('/stream/{filename}', 'StreamController@stream')->name('stream');
