<?php

namespace Tests\Feature;

use App\Ads;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Extras\AdCron;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdTest extends TestCase
{
    // use DatabaseTransactions;

    /** @test */
    public function insert_ad_crons()
    {


        Ads::create([
            'cover' => "dsa",
            "cover_type" => 'video',
            'content' => "das",
            'car_types' => [],
            "crons" => ["saturday-January-2020 00:30", "saturday-January-2020 00:30"]
        ]);

        $cronManager = new AdCron();

        $cronManager->execute();
    }

    /** @test */
    public function expire_cards()
    {

        $date = now()->addDays(5);

        factory('App\Card')->create(['valid_thru' => $date]);
    }
}
