<?php

use App\Card;
use App\CardRequest;
use App\CardType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CardTest extends TestCase
{
     use RefreshDatabase;

     /** @test */
     public function card_number_auto_generate()
     {


          $this->withoutExceptionHandling();


          $card = factory('App\Card')->raw();

          unset($card['number']);

          $card['auto_generate_number'] = true;

          //when creating new card with this type
          $response = $this->post(route('cards.store'), $card);

          $this->assertCount(1, Card::get());
     }
     /** @test */
     public function valid_thru_should_not_in_past()
     {


          $this->withoutExceptionHandling();


          $card = factory('App\Card')->raw(['valid_thru' => \Carbon\Carbon::now()->subMonths(3)]);

          unset($card['number']);

          $card['auto_generate_number'] = true;

          //when creating new card with this type
          $response = $this->post(route('cards.store'), $card);

          $response->assertStatus(500);

          $this->assertCount(0, Card::get());
     }
     /** @test */
     public function no_card_number_deplication()
     {


          factory('App\Card')->create(['number' => 1234]);

          $card = factory('App\Card')->raw(['number' => 1234]);

          //when creating new card with this type
          $response = $this->post(route('cards.store'), $card);

          $response->assertStatus(500);

          $this->assertCount(1, Card::get());
     }

     /** @test */
     public function if_no_number_in_request_return_error()
     {


          $card = factory('App\Card')->raw();

          unset($card['number']);

          //when creating new card with this type
          $response = $this->post(route('cards.store'), $card);

          $response->assertStatus(500);

          $this->assertCount(0, Card::get());
     }

     /** @test */
     public function create_new_card_on_request()
     {


          $cardrequest = factory('App\CardRequest')->create();


          $cardrequest->load(['user', 'cardType']);

          $card = factory('App\Card')->raw();

          $card['request_id'] = $cardrequest->id;

          //when creating new card with this type
          $response = $this->post(route('cards.store'), $card);

          $card = Card::find($response->json()['id']);

          $this->assertCount(0, CardRequest::get());
          $this->assertCount(1, Card::get());

          $this->assertEquals($card->user, $cardrequest->user);
          $this->assertEquals($card->cardType, $cardrequest->cardType);
     }
}
