<?php

use App\Card;
use App\CardType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CardTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_create_new_card_type()
    {


        //when creating new card with this type
        $this->post(route('new-card-type'), [
            'name' => 'card-name'
        ]);

        //should be exsist
        $this->assertCount(1, CardType::all());
    }

    /** @test */
    public function admin_can_create_card_for_a_user()
    {


        $this->withOutExceptionHandling();
        // we have a customer 
        $customer = factory('App\User')->create();

        //we have card type 1
        $cardType = factory('App\CardType')->create();

        //when creating new card with this type
        $this->post(route('user.new-card'), [
            'number' => rand(0, 1000),
            'user_id' => $customer->id,
            'type_id' => $cardType->id,
            'valid_thru' => Carbon\Carbon::now()
        ]);

        //should be linked to the user
        $this->assertCount(1, $customer->cards);

        $this->assertEquals($customer->cards()->first(), Card::first());
    }
}
