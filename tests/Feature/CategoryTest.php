<?php

use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{
     use RefreshDatabase;

     /** @test */
     public function admin_can_paginate_categories()
     {


          factory('App\Category', 20)->create();

          //when creating new card with this type
          $response = (object) (object) $this->get(route('category.index'))->json();


          $this->assertCount(10, $response->data);
     }

     /** @test */
     public function admin_can_view_category()
     {


          $category = factory('App\Category')->create();

          //when creating new card with this type
          $response = (object) $this->get(route('category.show', ['id' => $category->id]))->json();

          $this->assertEquals($category->id, $response->id);
     }
     /** @test */
     public function admin_can_paginate_category_merchants()
     {
          $categories = factory('App\Category', 3)->create();

          $merchants = factory('App\User', 2)->create(['type' => 'merchant']);

          $merchants->first()->categories()->attach($categories->first());

          $merchants->last()->categories()->attach($categories->last());

          //when creating new card with this type
          $response1 = (object) $this->get(route('category.merchants', ['id' => $categories->first()->id]))->json();


          $response2 = (object) $this->get(route('category.merchants', ['id' => $categories->last()->id]))->json();

          $response3 = (object) $this->get(route('category.merchants', ['id' => $categories->get(1)->id]))->json();

          $this->assertCount(1, $response1->data);

          $this->assertCount(1, $response2->data);

          $this->assertCount(0, $response3->data);
     }
     /** @test */
     public function admin_can_create_new_category()
     {


          //when creating new card with this type
          $this->post(route('category.store'), [
               'name' => 'category-1'
          ]);

          //should be exsist
          $this->assertCount(1, Category::all());
     }

     /** @test */
     public function admin_can_destroy_category()
     {

          $category = factory('App\Category')->create();

          $this->assertCount(1, Category::all());

          //when creating new card with this type
          $this->delete(route('category.destroy', ['id' => $category->id]));

          $this->assertCount(0, Category::all());
     }
}
