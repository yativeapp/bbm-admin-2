<?php

use App\Card;
use App\CardType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GeoTest extends TestCase
{
     use RefreshDatabase;

     /** @test */
     public function can_get_nearby_users()
     {

          $this->withOutExceptionHandling();

          $user = factory('App\User')->create([
               'type' => 'merchant',
               'latitude' => '30.0253184',
               'longitude' => '31.244288'
          ]);



          $this->post(route('geo.get'), ['lat' => '30.0253184', 'lng' => '31.244288']);
     }
}
