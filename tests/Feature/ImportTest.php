<?php

use App\Address;
use App\Card;
use App\CardType;
use App\MerchantCompany;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;

class ImportTest extends TestCase
{
  use DatabaseTransactions;


  /** @test */
  public function import_merchants()
  {
    factory(CardType::class)->create();

    $this->withoutExceptionHandling();
    Passport::actingAs(
      factory('App\User')->create(),
      []
    );
    // $this->withoutExceptionHandling();

    $path = storage_path('test\merchants2.xlsx');

    $file = new UploadedFile($path, 'merchants2.xlsx', filesize($path), null, null, true);


    $this->json('POST', '/import/merchants', [
      'file' => $file,
      'card_type' => CardType::first()->id
    ]);

    $this->assertCount(1, User::get());
    $this->assertCount(1, Address::get());
  }

  /** @test */
  public function import_users()
  {

    $this->withoutExceptionHandling();

    factory(CardType::class)->create();

    Passport::actingAs(
      factory('App\User')->create(),
      []
    );
    $this->withoutExceptionHandling();

    $path = storage_path('test\users2.xlsx');

    $file = new UploadedFile($path, 'users.xlsx', filesize($path), null, null, true);


    $this->json('POST', '/import/users', [
      'file' => $file,
      'card_type' => CardType::first()->id
    ]);

    $this->assertCount(1, User::get());
    $this->assertCount(1, Address::get());
  }

  /** @test */
  public function export_users()
  {


    $card = factory(Card::class)->create();


    Passport::actingAs(
      $card->user,
      []
    );
    $this->withoutExceptionHandling();



    $this->post('/export/users');
  }
  /** @test */
  public function export_merchants()
  {

    factory(User::class , 4)->create(['type' => 'merchant']);

    Passport::actingAs(
      factory('App\User')->create(),
      []
    );
    $this->withoutExceptionHandling();



    $this->post('/export/merchants');
  }

  /** @test */
  public function test_sos_offers_noti()
  {
    Passport::actingAs(
      factory('App\User')->create(),
      []
    );




    $offers = factory('App\Offer', 10)->create(['sos' => true]);

    $carType = factory('App\Cartype')->create(['name' => 'bm']);


    auth()->user()->carTypes()->sync($carType);

    $offers->load('merchant');


    $offers->pluck('merchant')->each(function ($merchant) use ($carType) {
      $merchant->carTypes()->attach($carType->id);
    });

    $this->withoutExceptionHandling();

    $card = factory('App\Card')->create(['user_id' => auth()->id()]);

    $cardType = $card->cardType;

    $discount = factory('App\OfferDiscount')->create(['card_type_id' => $cardType->id]);

    $discount->offer->cardTypes()->save($cardType);

    $this->get('user/coupons');
  }

  /** @test */
  public function geo_test_block()
  {
    Passport::actingAs(
      factory('App\User')->create(),
      []
    );

    // 1. blocked
    $x = auth()->user()->update(['blocked_to' => Carbon::now()->addMonth(1)]);
    $this->postJson('canRedeem')->assertExactJson([
      "can" => false,
      "reason" => "blocked",
      "data" => []
    ]);

    // 2. points
    $x = auth()->user()->update(['points' => 0]);
    $this->postJson('canRedeem')->assertExactJson([
      "can" => false,
      "reason" => "blocked",
      "data" => []
    ]);
  }

  /** @test */
  public function geo_test_points()
  {
    Passport::actingAs(
      factory('App\User')->create(),
      []
    );


    $x = auth()->user()->update(['points' => 0]);
    $this->postJson('canRedeem')->assertExactJson([
      "can" => false,
      "reason" => "not_enough_points",
      "data" => []
    ]);
  }
  /** @test */
  public function geo_test_in_membership()
  {
    Passport::actingAs(
      factory('App\User')->create(),
      []
    );


    $cardType = factory('App\CardType')->create(['type' => 'gift']);

    factory('App\Card')->create(['card_type_id' => $cardType, 'user_id' => auth()->id()]);

    $this->withoutExceptionHandling();

    $this->get('user/gifts');
  }
}
