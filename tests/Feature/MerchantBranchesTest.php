<?php

use App\Category;
use App\MerchantBranch;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MerchantBranchesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_paginate_merchant_branches()
    {

        $this->withOutExceptionHandling();


        $merchant = factory('App\User')->create(['type' => 'merchant']);

        $branches = factory('App\MerchantBranch', 10)->create(['merchant_id' => $merchant->id]);


        //when creating new card with this type
        $response = (object) $this->call('GET', route('merchant-branches.index'), ['merchant_id' => $merchant->id])->json();

        $this->assertCount(10, $response->data);
    }

    /** @test */
    public function admin_can_update_merchant()
    {
        $this->withoutExceptionHandling();

        $merchant = factory('App\User')->create(['type' => 'merchant']);

        $branch = factory('App\MerchantBranch')->create(['merchant_id' => $merchant->id, 'name' => 'branch'])->toArray();

        $this->assertEquals('branch',   $branch['name']);

        $branch['name'] = "bbm-bra";

        //when creating new card with this type
        $this->patch(route('merchant-branches.update', ['merchant_branch' => $branch['id']]), $branch);

        $branch = MerchantBranch::find($branch['id']);

        $this->assertEquals('bbm-bra',   $branch->name);
    }
    /** @test */
    public function admin_can_store_merchant_branch()
    {


        $merchant = factory('App\User')->create(['type' => 'merchant']);

        $branch = factory('App\MerchantBranch')->raw(['merchant_id' => $merchant->id]);


        //when creating new card with this type
        $this->post(route('merchant-branches.store', $branch));


        $this->assertCount(1, $merchant->branches);
    }

    /** @test */
    public function admin_can_destroy_merchant_branch()
    {

        $merchant = factory('App\User')->create(['type' => 'merchant']);

        $branches = factory('App\MerchantBranch', 10)->create(['merchant_id' => $merchant->id]);

        $this->assertCount(10, $merchant->branches);

        //when creating new card with this type
        $this->delete(route('merchant-branches.destroy', ['merchant_branch' => $branches->first()->id]));

        $this->assertNull(MerchantBranch::find($branches->first()->id));

        $this->assertCount(9, $merchant->branches()->get());
    }
}
