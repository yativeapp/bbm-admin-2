<?php

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class MerchantsTest extends TestCase
{
     use RefreshDatabase;

     /** @test */
     public function user_can_paginate_merchants()
     {


          factory('App\User', 20)->create(['type' => 'merchant']);

          //when creating new card with this type
          $response = (object) $this->get(route('merchants.index') . '?paginate=10')->json();

          $this->assertCount(10, $response->data);
     }

     /** @test */
     public function branches()
     {

          Passport::actingAs(
               factory('App\User')->create(),
               []
          );

          //when creating new card with this type
          $response = $this->get('/merchant/branches' . '?paginate=10')->json();

          dd($response);
     }

     /** @test */
     public function user_can_find_merchant_by_name()
     {


          $merchant =  factory('App\User')->create(['type' => 'merchant', 'fullname' => 'test1']);
          factory('App\User')->create(['type' => 'merchant', 'fullname' => 'should_not_appear']);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['name' => $merchant->fullname])->json();

          $this->assertCount(1, $response);

          $this->assertEquals('test1', $response[0]['fullname']);
     }
     /** @test */
     public function user_can_find_merchant_by_email()
     {


          $merchant =  factory('App\User')->create(['type' => 'merchant', 'email' => 'test1']);
          factory('App\User')->create(['type' => 'merchant', 'email' => 'should_not_appear']);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['email' => $merchant->email])->json();

          $this->assertCount(1, $response);

          $this->assertEquals('test1', $response[0]['email']);
     }
     /** @test */
     public function user_can_order_results()
     {


          factory('App\User')->create(['type' => 'merchant', 'fullname' => 'abc']);
          factory('App\User')->create(['type' => 'merchant', 'fullname' => 'def']);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['orderBy' => 'fullname,asc'])->json();


          $this->assertEquals('abc', $response[0]['fullname']);

          $response = $this->call('GET', route('merchants.index'), ['orderBy' => 'fullname,desc'])->json();


          $this->assertEquals('def', $response[0]['fullname']);
     }

     /** @test */
     public function user_can_limit_results()
     {


          $merchants =  factory('App\User', 30)->create(['type' => 'merchant', 'fullname' => 'test1']);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['name' => $merchants[0]->fullname, 'limit' => 5])->json();

          $this->assertCount(5, $response);

          $this->assertEquals('test1', $response[0]['fullname']);
     }
     /** @test */
     public function user_can_get_relation_count()
     {


          $merchants =  factory('App\User', 30)->create(['type' => 'merchant', 'fullname' => 'test1']);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['count' => 'branches'])->json();

          $this->assertNotNull($response[0]['branches_count']);
     }
     /** @test */
     public function user_can_filter_by_category()
     {


          $user = factory('App\User')->create(['type' => 'merchant']);

          factory('App\User', 20)->create(['type' => 'merchant']);

          $category = factory('App\Category')->create();

          $user->categories()->attach($category->id);

          //when creating new card with this type
          $response = $this->call('GET', route('merchants.index'), ['filterByCategory' => $category->id])->json();

          $this->assertCount(1, $response);
     }

     /** @test */
     public function admin_can_update_merchant()
     {

          $merchant = factory('App\User')->create(['type' => 'merchant', 'fullname' => 'bbm'])->toArray();

          $this->assertEquals('bbm',   $merchant['fullname']);

          $merchant['fullname'] = "bbm-mer";

          //when creating new card with this type
          $this->patch(route('merchants.patch', $merchant));

          $merchant = User::merchants()->find($merchant['id']);

          $this->assertEquals('bbm-mer',   $merchant->fullname);
     }
     /** @test */
     public function admin_can_store_merchant()
     {

          $this->withOutExceptionHandling();

          $merchant = factory('App\User')->raw(['type' => 'merchant', 'fullname' => 'bbm']);

          //when creating new card with this type
          $this->post(route('merchants.store', $merchant));


          $this->assertEquals(User::merchants()->first()->fullname,   $merchant['fullname']);
     }

     /** @test */
     public function admin_can_destroy_merchant()
     {

          $merchant = factory('App\User')->create(['type' => 'merchant']);

          $this->assertCount(1, User::merchants()->get());

          //when creating new card with this type
          $this->delete(route('merchants.destroy', ['id' => $merchant->id]));

          $this->assertCount(0, User::merchants()->get());
     }
}
