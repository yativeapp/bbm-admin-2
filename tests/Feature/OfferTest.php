<?php

use App\Card;
use App\CardRequest;
use App\CardType;
use App\Offer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class OfferTest extends TestCase
{
     use RefreshDatabase;

     /** @test */
     public function user_can_create_offer()
     {




          $offer = factory('App\Offer')->raw();


          $offer['discounts'] = factory('App\OfferDiscount', 4)->raw(['offer_id' => null]);




          //when creating new card with this type
          $response = $this->post(route('offers.store'), $offer);

          $offer = Offer::find($response->json()['id']);

          $this->assertCount(1, Offer::get());

          $this->assertCount(4, $offer->discounts);
     }

     /** @test */
     public function user_can_update_offer()
     {


          $this->withoutExceptionHandling();

          $offer = factory('App\Offer')->raw();


          $offer['discounts'] = factory('App\OfferDiscount', 4)->raw(['offer_id' => null, 'discount' => 40]);


          //when creating new card with this type
          $response = $this->post(route('offers.store'), $offer);

          $offerModel = Offer::find($response->json()['id']);


          $this->assertEquals(40,  $offerModel->discounts->first()->discount);


          //


          $offer = factory('App\Offer')->raw();



          $discounts = collect($offerModel->discounts->toArray())->map(function ($discount) {
               $discount['discount'] = 100;

               return $discount;
          });


          $offer['discounts'] = $discounts->toArray();

          $offer['discounts'][] = factory('App\OfferDiscount')->raw(['offer_id' => null, 'discount' => 40]);




          $this->patch(route('offers.update', ['offer' => $offerModel->id]), $offer);

          $this->assertEquals(100,  $offerModel->discounts->fresh()->last()->discount);
          $this->assertEquals(5, Offer::find($response->json()['id'])->discounts()->count());


          $offer['discounts'] = [];

          $this->patch(route('offers.update', ['offer' => $offerModel->id]), $offer);

          $this->assertEquals(0, Offer::find($response->json()['id'])->discounts()->count());
     }

     public function claim()
     {

          Passport::actingAs(
               factory('App\User')->create(),
               []
          );
           
     }
}
