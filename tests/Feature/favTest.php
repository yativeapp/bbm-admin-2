<?php

use App\Card;
use App\CardRequest;
use App\CardType;
use App\Offer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Laravel\Passport\Passport;

class favTest extends TestCase
{
     use RefreshDatabase;


     /** @test */
     public function can_fav()
     {


          $this->withoutExceptionHandling();


          Passport::actingAs(
               factory('App\User')->create(),
               []
          );
          $this->withOutExceptionHandling();

          $offer = factory('App\Offer')->create();

          $this->post('/favourite', ['offer_id' => $offer->id]);

          $response = $this->get('/favourite')->json();
     }
}
